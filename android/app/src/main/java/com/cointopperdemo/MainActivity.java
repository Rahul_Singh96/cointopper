package com.cointopperdemo;
import android.content.Intent;     // <--- import
// import android.os.Bundle;

// import com.facebook.CallbackManager;
// import com.facebook.FacebookSdk;
// import com.facebook.reactnative.androidsdk.FBSDKPackage;
// import com.facebook.appevents.AppEventsLogger;
import com.facebook.react.ReactActivity;

public class MainActivity extends ReactActivity {
    // CallbackManager mCallbackManager;
    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    // @Override
    // protected List<ReactPackage> getPackages() {
    //     mCallbackManager = new CallbackManager.Factory().create();
    //     ReactPackage packages[] = new ReactPackage[]{
    //         new MainReactPackage(),
    //         new FBSDKPackage(mCallbackManager),
    //     };
    //     return Arrays.<ReactPackage>asList(packages);
    // }

    // @Override
    // protected void onCreate(Bundle savedInstanceState) {
    //     super.onCreate(savedInstanceState);
    //     FacebookSdk.sdkInitialize(getApplicationContext());
    // } 

    // @Override
    // public void onActivityResult(int requestCode, int resultCode, Intent data) {
    //     mCallbackManager.onActivityResult(requestCode, resultCode, data);
    // }

    // @Override
    // protected void onResume() {
    //     super.onResume();
    //     AppEventsLogger.activateApp(getApplicationContext());
    // }

    // @Override
    // protected void onPause() {
    //     super.onPause();
    //     AppEventsLogger.deactivateApp(getApplicationContext());
    // }

    // @Override
    // protected void onStop() {
    //     super.onStop();
    //     AppEventsLogger.onContextStop();
    // }

    @Override
    protected String getMainComponentName() {
        return "CoinTopperDemo";
    }

    @Override
     public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    MainApplication.getCallbackManager().onActivityResult(requestCode, resultCode, data);
    }
}
