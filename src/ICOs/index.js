import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator, } from 'react-navigation';
import Home from './Home';
import search from '../Components/Search';
import WatchList from '../Components/WatchList';
const IcoHome = createStackNavigator(
    {
        Home: {
            screen: Home,
            navigationOptions: {
                header: null,
            },
        },
        Search: {
            screen: search,
            navigationOptions: {
                header: null,
            },
        },
        Watchlist: {
            screen: WatchList,
            navigationOptions: {
                header: null,
            },
        },
    },
    {
        // navigationOptions: ({ navigation }) => ({
        //     header: null,
        //     tabBarVisible: false,
        // }),
    },
);

IcoHome.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        return {
        tabBarVisible: false
      };
    }
  
    return {
      tabBarVisible,
    };
  };

export default IcoHome;