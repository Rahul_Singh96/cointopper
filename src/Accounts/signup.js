import React, { Component } from 'react';
import {
    View,
    SafeAreaView,
    ScrollView,
    Dimensions,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    Modal,
} from 'react-native';
import AutoHeightWebView from 'react-native-autoheight-webview';
import LinearGradient from 'react-native-linear-gradient';
import { sha256 } from 'react-native-sha256';

const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
} = Dimensions.get('window');

export default class signup extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: true,
            SignUp: [],
            modalVisible: false,
            screenHeight: SCREEN_HEIGHT,
            Social_ID: "",
            Email: "",
            Password: "",
            ConfirmPass: "",
            Photo: "",
            Name: "",
            Logintype: "",
        }
    };

    // componentDidMount() {
    //     this.SignUpData();
    // };

    // // For Terms Of Use
    // SignUpData = ( Email, Password ) => {
    //     sha256(Password).then( hash => {
    //         console.log("Enc Pass:-"+JSON.stringify(hash));
    //         // this.setState({
    //         //   sha256: hash
    //         // })
    //         const url = 'https://api.cointopper.com/api/v3/signup';
    //     fetch(url, {
    //         method: 'POST',
    //         body: JSON.stringify({email: Email, password: hash})
    //       })
    //         .then((response) => response.json())
    //         .then((responseJson) => {
    //             console.log("data==>>" + JSON.stringify(responseJson));
    //             // console.log("Password==>>" + Password);
    //         })
    //         .catch((error) => {
    //             console.log(error)
    //         })
    //     })
        
    // }

    setModalVisibleTermOfUse(visible) {
        this.setState({ modalVisible: visible });
    };

    closeModal(visible) {
        this.setState({
            modalVisible: visible,
        });
    };

    render() {
        const { SignUp } = this.state;
        const { navigate } = this.props.navigation;
        const { navigation, goBack, } = this.props;
        const key = navigation.getParam('SignUpTermOfUse', 'NO-Symbol');
        // console.log("key TermOfUseData SignUp ==>> "+ JSON.stringify(key));
        console.log("SignUp==>>" + JSON.stringify(SignUp));

        return (
            <View style={{ flex: 1 }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <LinearGradient
                        colors={['#003399', '#0073e6']}
                        start={{ x: 0, y: 1 }}
                        end={{ x: 1, y: 0 }}
                        style={{ flex: 1 }}
                    >
                        <View style={{
                            height: 100,
                            alignItems: 'flex-start',
                            flexDirection: 'column',
                            padding: 10
                        }}>
                            {/* Lable Component */}
                            <View style={{ flex: 1, alignItems: 'flex-start', flexDirection: 'row' }}>
                                <View style={{ flexDirection: 'column', alignItems: 'stretch' }}>
                                    <Text style={{ fontSize: 17, fontStyle: 'normal', color: '#f2f2f2' }}>
                                        Welcome to CoinTopper
                                    </Text>
                                    <Text style={{
                                        fontSize: SCREEN_WIDTH * 0.09,
                                        fontWeight: 'bold',
                                        fontStyle: 'normal',
                                        color: '#ffffff'
                                    }}>
                                        Sign in
                                    </Text>
                                </View>
                            </View>
                        </View>

                        <View style={{ padding: 10, }}>
                            <View style={{ height: SCREEN_WIDTH * 0.9 }}>
                                <View style={styles.coinCapBoxStyle} >
                                    <TextInput style={styles.TextInputStyle}
                                        underlineColorAndroid="#0073e6"
                                        placeholder="Email:"
                                        placeholderTextColor="#0073e6"
                                        autoCapitalize="none"
                                        // value={this.state.Email}
                                        onChangeText={() => this.SignUpData}
                                    />

                                    <TextInput style={styles.TextInputStyle}
                                        underlineColorAndroid="#0073e6"
                                        placeholder="Password: (6 - 20 char)"
                                        placeholderTextColor="#0073e6"
                                        autoCapitalize="none"
                                        secureTextEntry={true}
                                        // value={this.state.Password}
                                        onChangeText={() => this.SignUpData}
                                    />

                                    <TextInput style={styles.TextInputStyle}
                                        underlineColorAndroid="#0073e6"
                                        placeholder="Confirm Password: (6 - 20 char)"
                                        placeholderTextColor="#0073e6"
                                        autoCapitalize="none"
                                        secureTextEntry={true}
                                        // value={this.state.ConfirmPass}
                                        onChangeText={() => this.SignUpData}
                                    />
                                   
                                    <TouchableOpacity
                                        style={styles.buttonStyle}
                                        onPress={
                                            () => this.SignUpData(this.state.email, this.state.password, this.state.ConfirmPass)
                                        }>
                                        <Text style={{ padding: 7, color: "#ffffff", alignSelf: "center" }}> SIGN IN </Text>
                                    </TouchableOpacity>
                                    <View style={{ flex: 1, flexDirection: "row" }}>
                                        <View style={{ width: "60%" }}>
                                            <Text style={{ alignSelf: "flex-end", position: 'absolute', bottom: 15, }}>
                                                Not having account?
                                            </Text>
                                        </View>
                                        <View style={{ width: "40%" }}>
                                            <Text style={{
                                                color: "#0073e6",
                                                alignSelf: 'flex-start',
                                                position: 'absolute',
                                                bottom: 15,
                                                paddingLeft: 10,
                                            }}
                                                onPress={() => this.props.navigation.navigate('Login')}
                                            >
                                                Sign in
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>

                        <View style={{ alignSelf: "center", margin: 20, }}>
                            <Text style={{ fontSize: 14, color: "#ffffff" }}> or sign In using </Text>
                        </View>

                        <View style={{ flexDirection: "row", justifyContent: "space-around", alignSelf: "center" }}>
                            <View style={{ flex: 1, padding: 10 }}>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate('Login')}
                                    style={{ width: "100%", height: 50, borderRadius: 10, backgroundColor: '#009933', justifyContent: 'center' }}
                                >
                                    <Text style={{ alignSelf: 'center', color: '#ffffff' }}> Login </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1, padding: 10 }}>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate('Login')}
                                    style={{ width: "100%", height: 50, borderRadius: 10, backgroundColor: '#009933', justifyContent: 'center' }}
                                >
                                    <Text style={{ alignSelf: 'center', color: '#ffffff' }}> Login </Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View>
                            <Modal
                                animationType="fade"
                                transparent={true}
                                visible={this.state.modalVisible}
                                onRequestClose={this.closeModal}
                            >
                                <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.5)', }}>
                                    <View style={{
                                        height: "90%",
                                        width: "90%",
                                        alignSelf: 'center',
                                        backgroundColor: "white",
                                        padding: 10,
                                        marginTop: 20,
                                    }}>
                                        <ScrollView
                                            style={{ height: SCREEN_WIDTH * 0.7, padding: 10 }}
                                            contentContainerStyle={{ backgroundColor: 'white', flexGrow: 1, }}
                                            onContentSizeChange={this.onContentSizeChange}
                                        >
                                            <View style={{ padding: 10, alignSelf: "center" }}>
                                                <AutoHeightWebView
                                                    style={{ width: Dimensions.get('window').width - 100, padding: 10 }}
                                                    customScript={`document.body.style.background = 'white';`}
                                                    customStyle={`
                                                                    * {
                                                                        font-family: 'Times New Roman';
                                                                    }
                                                                    p {
                                                                        font-size: 16px;
                                                                    }
                                                                `}
                                                    onSizeUpdated={size => console.log(size.height)}
                                                    files={[{
                                                        href: 'cssfileaddress',
                                                        type: 'text/css',
                                                        rel: 'stylesheet'
                                                    }]}
                                                    source={{ html: key.text }}
                                                    zoomable={false}
                                                />
                                            </View>
                                        </ScrollView>
                                        {/* Button Cancel And Save */}
                                        <View style={{ paddingRight: 20, flexDirection: "row", justifyContent: "space-between" }}>
                                            <View style={{ flex: 1, padding: 10 }}>
                                                <TouchableOpacity
                                                    style={{ alignSelf: 'center' }}
                                                    onPress={() => this.closeModal(false)}
                                                >
                                                    <Text style={{ color: "#0000ff", fontSize: 15 }}>Cancel</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </Modal>
                        </View>

                        <View style={{ flex: 1, flexDirection: "row" }}>
                            <View style={{ width: "60%" }}>
                                <Text style={{ alignSelf: "flex-end", position: 'absolute', bottom: 15, color: "#ffffff" }}>
                                    By using this app you agree our
                                            </Text>
                            </View>
                            <View style={{ width: "40%" }}>
                                <Text style={{
                                    color: "#ffffff",
                                    alignSelf: 'flex-start',
                                    position: 'absolute',
                                    bottom: 15,
                                    paddingLeft: 10,
                                    textDecorationLine: "underline"
                                }}
                                    onPress={() => {
                                        this.setModalVisibleTermOfUse(true);
                                    }}
                                >
                                    Terms of use
                                </Text>
                            </View>
                        </View>


                    </LinearGradient>
                </SafeAreaView>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    buttonStyle: {
        width: "90%",
        height: 50,
        alignSelf: "center",
        borderRadius: 12,
        shadowOpacity: 10,
        padding: 10,
        backgroundColor: '#0000ff',
        position: 'absolute',
        bottom: 30,
        marginBottom: 10
    },
    coinCapBoxStyle: {
        flex: 1,
        borderRadius: 10,
        elevation: 1,
        flexDirection: "column",
        overflow: 'hidden',
        backgroundColor: "#ffffff",
        // padding:10,
    },
    TextInputStyle: {
        borderRadius: 10,
        margin: 8,
        height: 50,
        width: "90%",
        fontSize: 18
    },
})