import React, { Component } from 'react';
import {
    View,
    SafeAreaView,
    ScrollView,
    Dimensions,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    Modal,
    Alert,
    Platform,
} from 'react-native';
import AutoHeightWebView from 'react-native-autoheight-webview';
import LinearGradient from 'react-native-linear-gradient';
import { sha256 } from 'react-native-sha256';
import DeviceInfo from 'react-native-device-info';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import { LoginManager, LoginButton, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';

const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
} = Dimensions.get('window');

export default class signin extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: true,
            userInfo: [],
            modalVisible: false,
            modalVisibleForgetPass: false,
            screenHeight: SCREEN_HEIGHT,
            Email: "",
            Password: "",
            Social_ID: "",
            Photo: "",
            Name: "",
            Logintype: "",
            sha256: "",
        }
        this._signIn.bind(this);
    };

    componentDidMount() {
        GoogleSignin.configure({
            scopes: ['https://www.googleapis.com/auth/gmail.labels'],
            webClientId: '579067266795-ph8u60kp493ndqenluuq3qe7lpho5ivp.apps.googleusercontent.com',
            offlineAccess: true,
            hostedDomain: '',
            forceConsentPrompt: true,
        });
    };

    // Sign In With Google 
    _signIn = async () => {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            this.setState({ userInfo: userInfo, loggedIn: true, logintype: 3 });
            // console.log(" _signIn userInfo==> " + JSON.stringify(userInfo))
            // console.log("Google ID:-" + userInfo.user.id);
            // console.log("Google Email:-" + userInfo.user.email);
            // console.log("Google Photo:-" + userInfo.user.photo);
            // console.log("Google Name:-" + userInfo.user.name);
            this.LoginFunc(userInfo.user.id, userInfo.user.email, "", userInfo.user.photo, userInfo.user.name, 3)
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.log("user cancelled the login flow" + error)
            } else if (error.code === statusCodes.IN_PROGRESS) {
                console.log("operation (f.e. sign in) is in progress already" + error)
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                console.log("play services not available or outdated" + error)
            } else {
                console.log("some other error happened" + error)
            }
        }
    };

    getCurrentUserInfo = async () => {
        try {
            const userInformation = await GoogleSignin.signInSilently();
            this.setState({ userInformation });
            console.log("getCurrentUserInfo userInfo==> " + userInformation)
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_REQUIRED) {
                console.log(" user has not signed in yet")
                this.setState({ loggedIn: false });
            } else {
                console.log("some other error")
                this.setState({ loggedIn: false });
            }
        }
    };

    signOut = async () => {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            this.setState({ user: null, loggedIn: false }); // Remember to remove the user from your app's state as well
        } catch (error) {
            console.error(error);
        }
    };

    // Sign In With Email and Password
    LoginFunc = (SocialID, Email, Password, PhotoUrl, Name, Logintype) => {
        // console.log("SocialID:->" + SocialID);
        // console.log("Email:->" + Email);
        // console.log("Password:->" + Password);
        // console.log("PhotoURL:->" + PhotoUrl);
        // console.log("Name:->" + Name);
        // console.log("LoginType:->" + Logintype);
        const { navigate } = this.props.navigation;
        fetch('https://api.cointopper.com/api/v3/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                socialuid: SocialID,
                photo: PhotoUrl,
                deviceid: DeviceInfo.getUniqueID(),
                name: Name,
                devtype: Platform.OS === "ios" ? 1 : 2,
                logintype: Logintype,
                email: Email,
                password: Password != "" ? Password : "",
            })
        })
            .then((response) => response.json())
            .then((responseJson) => {
                // console.log("Login Response Status:-" + JSON.stringify(responseJson));
                AsyncStorage.setItem('UserData', JSON.stringify(responseJson.data));
                if (responseJson.status === 1) {
                    console.log('Success! You are log in.');
                    // this.props.navigation.navigate('Account');
                    this.props.navigation.goBack();
                } else {
                    console.log('There was an error login your account.');
                }
                this._retrieveData();
            })
            .catch((error) => {
                console.log("Error=>> " + error);
            })
    };

    // storeValue = async (data) => {
    //     try {
    //         await AsyncStorage.setItem('UserData', JSON.stringify(data));
    //     } catch (error) {
    //         // Error saving data
    //     }
    // }

    _retrieveData = async () => {
        try {
            const value = await AsyncStorage.getItem('UserData');
            if (value !== null) {
                // We have data!!
                // console.log(value);
            }
        } catch (error) {
            console.log("error=>> "+ error )
            // Error retrieving data
        }
    };

    // visible For TermOfUse
    setModalVisibleTermOfUse(visible) {
        this.setState({ modalVisible: visible });
    };

    // visible For ForgetPass
    setModalVisibleForgetPass(visible) {
        this.setState({ modalVisibleForgetPass: visible });
    };

    closeModal(visible) {
        this.setState({
            modalVisible: visible,
            modalVisibleForgetPass: visible,
        });
    };

    // Sign In With Facebook
    onLoginWithFacebook = () => {
        LoginManager.logInWithReadPermissions(["public_profile", "email"]).then(
            (result) => {
                if (result.isCancelled) {
                    console.log("Login cancelled");
                } else {
                    console.log(
                        "Login success with permissions: " +
                        result.grantedPermissions.toString()
                    );
                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            let accessToken = data.accessToken;
                            // alert(accessToken.toString());
                            const responseInfoCallback = (error, result) => {
                                if (error) {
                                    console.log('Error fetching data: ' + error);
                                    // alert('Error fetching data: ' + error.toString());
                                } else {
                                    console.log('Success fetching data: ' + result);
                                    // alert('Success fetching data: ' + result.toString());
                                    console.log("Facebook ID:-" + result.id);
                                    console.log("Facebook email:-" + result.email);
                                    console.log("Facebook photo:-" + result.picture.data.url);
                                    console.log("Facebook name:-" + result.name);
                                    this.LoginFunc(result.id, result.email, "", result.picture.data.url, result.name, 2);
                                }
                            }
                            const infoRequest = new GraphRequest(
                                '/me',
                                {
                                    accessToken: accessToken,
                                    parameters: {
                                        fields: {
                                            string: 'email,name,first_name,middle_name,last_name,picture.type(large)'
                                        }
                                    }
                                },
                                responseInfoCallback
                            );
                            // Start the graph request.
                            new GraphRequestManager().addRequest(infoRequest).start();
                        })
                }
            },
            (error) => {
                console.log("Login fail with error: " + error);
            }
        );
    }

    render() {
        const { navigate } = this.props.navigation;
        const { navigation, goBack, } = this.props;
        const key = navigation.getParam('TermOfUseData', 'NO-Symbol');
        // console.log("key ==>>" + JSON.stringify(key));
        return (
            <View style={{ flex: 1 }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <LinearGradient
                        colors={['#003399', '#0073e6']}
                        start={{ x: 0, y: 1 }}
                        end={{ x: 1, y: 0 }}
                        style={{ flex: 1 }}
                    >
                        <View style={{
                            height: 100,
                            alignItems: 'flex-start',
                            flexDirection: 'column',
                            padding: 10
                        }}>
                            {/* Lable Component */}
                            <View style={{ flex: 1, alignItems: 'flex-start', flexDirection: 'row' }}>
                                <View style={{ flexDirection: 'column', alignItems: 'stretch' }}>
                                    <Text style={{ fontSize: 17, fontStyle: 'normal', color: '#f2f2f2' }}>
                                        Welcome to CoinTopper
                                    </Text>
                                    <Text style={{
                                        fontSize: SCREEN_WIDTH * 0.09,
                                        fontWeight: 'bold',
                                        fontStyle: 'normal',
                                        color: '#ffffff'
                                    }}>
                                        Sign in
                                    </Text>
                                </View>
                            </View>

                        </View>

                        {/* Eamil, Password, Forget PassWord, 
                        Sign In TouchableOpacity, 
                        Not having Account? Sign Up TouchableOpacity */}
                        <View style={{ padding: 10, }}>
                            <View style={{ height: SCREEN_WIDTH * 0.9 }}>
                                {/* TextInput Email, PassWord, Forget Password, Sign In, Sign Up */}
                                <View style={styles.coinCapBoxStyle} >
                                    <TextInput style={styles.TextInputStyle}
                                        underlineColorAndroid="#0073e6"
                                        placeholder="Email:"
                                        placeholderTextColor="#0073e6"
                                        autoCapitalize="none"
                                        onChangeText={(text) => this.setState({ Email: text })}
                                    />
                                    <TextInput style={styles.TextInputStyle}
                                        underlineColorAndroid="#0073e6"
                                        placeholder="Password: (6 - 20 char)"
                                        placeholderTextColor="#0073e6"
                                        autoCapitalize="none"
                                        secureTextEntry={true}
                                        onChangeText={(Password) => this.setState({ Password })}
                                    />

                                    {/* forget password modal */}
                                    <View>
                                        <Modal
                                            animationType="fade"
                                            transparent={true}
                                            visible={this.state.modalVisibleForgetPass}
                                            onRequestClose={this.closeModal}
                                        >
                                            <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.5)' }}>
                                                <TouchableOpacity
                                                    style={{ height: "30%", width: "100%" }}
                                                    onPress={() => this.closeModal(false)}
                                                >
                                                    <Text>  </Text>
                                                </TouchableOpacity>
                                                <View style={{
                                                    height: "40%",
                                                    width: "90%",
                                                    alignSelf: 'center',
                                                    backgroundColor: "white",
                                                    padding: 10,
                                                    marginTop: 0,
                                                    justifyContent: "space-around",
                                                }}>
                                                    <Text style={{ textAlign: "center", color: "#ff0000" }}> Forget your password?</Text>
                                                    <Text style={{ textAlign: "center", color: "#0066ff" }}>
                                                        Enter your registered email address, we will send you a link to reset password
                                                    </Text>

                                                    <TextInput
                                                        style={{
                                                            backgroundColor: '#f2f2f2',
                                                            borderRadius: 10,
                                                            width: "70%",
                                                            height: 40,
                                                            alignSelf: 'center'
                                                        }}
                                                        underlineColorAndroid='transparent'
                                                        placeholder="Email Id:"
                                                        placeholderTextColor="#0073e6"
                                                        autoCapitalize="none"
                                                        onChangeText={this.state.Email}
                                                    />

                                                    {/* Button Cancel And Save */}
                                                    <View style={{ paddingRight: 20, flexDirection: "row", justifyContent: "space-between" }}>
                                                        <View style={{ flex: 1, padding: 10 }}>
                                                            <TouchableOpacity
                                                                style={{
                                                                    backgroundColor: '#0073e6',
                                                                    borderRadius: 10,
                                                                    width: "70%",
                                                                    color: "#000000",
                                                                    height: 40,
                                                                    alignSelf: 'center',
                                                                }}
                                                                onPress={() => this.closeModal(false)}
                                                            >
                                                                <Text style={{ color: "#000000", fontSize: 15, textAlign: 'center' }}>Send Email</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>
                                                </View>
                                                <TouchableOpacity
                                                    style={{ height: "30%", width: "100%" }}
                                                    onPress={() => this.closeModal(false)}
                                                >
                                                    <Text>  </Text>
                                                </TouchableOpacity>
                                            </View>
                                        </Modal>
                                    </View>

                                    {/* Forget PAssword Text Button */}
                                    <Text
                                        style={{ alignSelf: 'center', position: 'absolute', bottom: 100, }}
                                        onPress={() => {
                                            this.setModalVisibleForgetPass(true);
                                        }}
                                    >
                                        Forgot Password?
                                    </Text>

                                    {/* Sign In TouchableOpacity */}
                                    <TouchableOpacity
                                        style={styles.buttonStyle}
                                        onPress={
                                            () => {
                                                if (this.state.Password != "") {
                                                    sha256(this.state.Password).then((hash) => {
                                                        console.log("sha256==>>" + hash)
                                                        this.LoginFunc("", this.state.Email, hash, "", "", 1)
                                                    })
                                                }
                                            }
                                        }>
                                        <Text style={{ padding: 7, color: "#ffffff", alignSelf: "center" }}> SIGN IN </Text>
                                    </TouchableOpacity>

                                    {/* Not having account? Sign Up Page Butto  */}
                                    <View style={{ flex: 1, flexDirection: "row" }}>
                                        <View style={{ width: "60%" }}>
                                            <Text style={{ alignSelf: "flex-end", position: 'absolute', bottom: 15, }}>
                                                Not having account?
                                            </Text>
                                        </View>
                                        <View style={{ width: "40%" }}>
                                            <Text style={{
                                                color: "#0073e6",
                                                alignSelf: 'flex-start',
                                                position: 'absolute',
                                                bottom: 15,
                                                paddingLeft: 10,
                                            }}
                                                onPress={() => this.props.navigation.navigate('SignUp', {
                                                    SignUpTermOfUse: key,
                                                })}
                                            >
                                                Sign Up
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>

                        <View style={{ alignSelf: "center", margin: 20, }}>
                            <Text style={{ fontSize: 14, color: "#ffffff" }}> or sign In using </Text>
                        </View>

                        {/* Google Login TouchableOpacity and Facebook Login TouchableOpacity */}
                        <View style={{ flexDirection: "row", justifyContent: "space-around", alignSelf: "center" }}>

                            {/* Google Login TouchableOpacity */}
                            <View style={{ flex: 1, padding: 10 }}>
                                {/* <GoogleSigninButton
                                    style={{ width: "100%", height: 50, }}
                                    size={GoogleSigninButton.Size.Icon}
                                    color={GoogleSigninButton.Color.Dark}
                                    onPress={this._signIn.bind(this)}
                                /> */}
                                <TouchableOpacity
                                    onPress={this._signIn.bind(this)}
                                    style={{ width: "100%", height: 45, borderRadius: 10, backgroundColor: '#ff0000', justifyContent: 'center', borderWidth: 1.5, borderColor: "#ffffff" }}
                                >
                                    <View style={{ flexDirection: 'row', padding: 10 }}>
                                        <View style={{ width: "30%" }}>
                                            <Icon
                                                name='google'
                                                style={{ alignSelf: 'center' }}
                                                size={33}
                                                color='rgba(255, 255, 255, 1)'
                                                backgroundColor='#f2f2f2'
                                            />
                                        </View>
                                        <View style={{ width: "70%", justifyContent: 'center' }}>
                                            <Text style={{ alignSelf: 'center', color: '#ffffff', fontSize: 18 }}> Google </Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>

                            {/* Facebook Login TouchableOpacity */}
                            <View style={{ flex: 1, padding: 10 }}>
                                <TouchableOpacity
                                    style={{ width: "100%", height: 45, borderRadius: 10, backgroundColor: '#002db3', justifyContent: 'center', borderWidth: 1.5, borderColor: "#ffffff" }}
                                    onPress={this.onLoginWithFacebook}
                                >
                                    <View style={{ flexDirection: 'row', padding: 10 }}>
                                        <View style={{ width: "30%" }}>
                                            <Icon
                                                name='facebook'
                                                style={{ alignSelf: 'center' }}
                                                size={33}
                                                color='rgba(255, 255, 255, 1)'
                                                backgroundColor='#f2f2f2'
                                            />
                                        </View>
                                        <View style={{ width: "70%", justifyContent: 'center' }}>
                                            <Text style={{ alignSelf: 'center', color: '#ffffff', fontSize: 18 }}> Facebook </Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>

                        {/* Modal for Term Of Use */}
                        <View>
                            <Modal
                                animationType="fade"
                                transparent={true}
                                visible={this.state.modalVisible}
                                onRequestClose={this.closeModal}
                            >
                                <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.5)' }}>
                                    <View style={{
                                        height: "90%",
                                        width: "90%",
                                        alignSelf: 'center',
                                        backgroundColor: "white",
                                        padding: 10,
                                        marginTop: 20,
                                    }}>
                                        <ScrollView
                                            style={{ height: SCREEN_WIDTH * 0.7, padding: 10 }}
                                            contentContainerStyle={{ backgroundColor: 'white', flexGrow: 1, }}
                                            onContentSizeChange={this.onContentSizeChange}
                                        >
                                            <View style={{ padding: 10, alignSelf: "center" }}>
                                                <AutoHeightWebView
                                                    style={{ width: Dimensions.get('window').width - 100, padding: 10 }}
                                                    customScript={`document.body.style.background = 'white';`}
                                                    customStyle={`
                                                                    * {
                                                                        font-family: 'Times New Roman';
                                                                    }
                                                                    p {
                                                                        font-size: 16px;
                                                                    }
                                                                `}
                                                    onSizeUpdated={size => console.log(size.height)}
                                                    files={[{
                                                        href: 'cssfileaddress',
                                                        type: 'text/css',
                                                        rel: 'stylesheet'
                                                    }]}
                                                    source={{ html: key.text }}
                                                    zoomable={false}
                                                />
                                            </View>
                                        </ScrollView>
                                        {/* Button Cancel */}
                                        <View style={{ paddingRight: 20, flexDirection: "row", justifyContent: "space-between" }}>
                                            <View style={{ flex: 1, padding: 10 }}>
                                                <TouchableOpacity
                                                    style={{ alignSelf: 'center' }}
                                                    onPress={() => this.closeModal(false)}
                                                >
                                                    <Text style={{ color: "#0000ff", fontSize: 15 }}>Cancel</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </Modal>
                        </View>

                        {/* Term of Use TouchableOpacity */}
                        <View style={{ flex: 1, flexDirection: "row" }}>
                            <View style={{ width: "60%" }}>
                                <Text style={{ alignSelf: "flex-end", position: 'absolute', bottom: 15, color: "#ffffff" }}>
                                    By using this app you agree our
                                </Text>
                            </View>
                            <View style={{ width: "40%" }}>
                                <Text style={{
                                    color: "#ffffff",
                                    alignSelf: 'flex-start',
                                    position: 'absolute',
                                    bottom: 15,
                                    paddingLeft: 10,
                                    textDecorationLine: "underline"
                                }}
                                    onPress={() => {
                                        this.setModalVisibleTermOfUse(true);
                                    }}
                                >
                                    Terms of use
                                </Text>
                            </View>
                        </View>

                    </LinearGradient>
                </SafeAreaView>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    buttonStyle: {
        width: "90%",
        height: 50,
        alignSelf: "center",
        borderRadius: 12,
        shadowOpacity: 10,
        padding: 10,
        backgroundColor: '#0000ff',
        position: 'absolute',
        bottom: 30,
        marginBottom: 10
    },
    coinCapBoxStyle: {
        flex: 1,
        borderRadius: 10,
        elevation: 1,
        flexDirection: "column",
        overflow: 'hidden',
        backgroundColor: "#ffffff",
        // padding:10,
    },
    TextInputStyle: {
        borderRadius: 10,
        margin: 8,
        height: 50,
        width: "90%",
        fontSize: 18
    },
})