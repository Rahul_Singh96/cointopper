import React, { Component } from 'react';
import { createStackNavigator, } from 'react-navigation';
import Accounts from './Accounts';
import signin from './signin';
import signup from './signup';
import managenotification from './managenotification';

const Account = createStackNavigator(
    {
        Account: {
            screen: Accounts,
            navigationOptions: {
                header: null,
            },
        },
        Login: {
            screen: signin,
            navigationOptions: {
                header: null,
            },
        },
        SignUp: {
            screen: signup,
            navigationOptions: {
                header: null,
            },
        },
        ManageNotification: {
            screen: managenotification,
            navigationOptions: {
                header: null,
            },
        },
        
    }, 
  {
    // navigationOptions: ({ navigation }) => ({   
    //     header: null,
    //     tabBarVisible: false,
    //   }),
  },
);

Account.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        return {
        tabBarVisible: false
      };
    }
  
    return {
      tabBarVisible,
    };
  };

export default Account;