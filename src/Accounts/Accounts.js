import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    SafeAreaView,
    TouchableOpacity,
    Dimensions,
    Modal,
    Linking,
    Image,
} from 'react-native';
import AutoHeightWebView from 'react-native-autoheight-webview';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/AntDesign';
import { NavigationEvents } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';

const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
} = Dimensions.get('window');

export default class Account extends Component {

    constructor() {
        console.log("constructor");
        super();
        this.state = {
            isLoading: true,
            TermOfUse: [],
            PrivacyPolicy: [],
            selectedStringData: [],
            modalVisible: false,
            screenHeight: SCREEN_HEIGHT,
            Visible: false,
            device_Access_Token: "",
            UName: "",
            UPhoto: ""
        }
    };

    componentDidMount() {
        console.log("componentDidMount");
        this.getListCoin();
    };

    // For Terms Of Use
    getListCoin = () => {
        console.log("Term Of Use");
        const url = 'https://api.cointopper.com/api/v3/termsofuse';
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    TermOfUse: responseJson.data
                })
            })
            .catch((error) => {
                console.log(error)
            })
    }

    // componentDidUpdate() {
    //     console.log("Account componentDidUpdate");
    // }

    // componentWillUpdate() {
    //     console.log("Account componentWillUpdate");
    // }

    // componentWillReceiveProps(props) {
    //     console.log("Account componentWillReceiveProps");
    //     this._retrieveData();
    // }

    // componentDidCatch() {
    //     console.log("Account componentDidCatch");
    // }

    // shouldComponentUpdate() {
    //     // this._retrieveData();
    //     console.log("Account shouldComponentUpdate");
    // }

    // For Privacy Policy
    componentWillMount() {
        console.log("Privacy Policy")
        const url = 'https://api.cointopper.com/api/v3/privacypolicy'
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    PrivacyPolicy: responseJson.data
                })
            })
            .catch((error) => {
                console.log(error)
            })
    };

    setModalVisibleTermOfUse(visible) {
        this.setState({ modalVisible: visible });
    };

    closeModal(visible) {
        this.setState({
            modalVisible: visible,
        });
    };

    PPAndTermOfUse = (data) => {
        this.setState({
            selectedStringData: data
        })
        this.setModalVisibleTermOfUse(true);
    }

    onContentSizeChange = (contentWidth, contentHeight) => {
        this.setState({ screenHeight: contentHeight });
    };

    _retrieveData = async () => {
        console.log("AscyncStorage Retrive Data");
        try {

            // AsyncStorage.getItem("UserData").then((value) => {
            //     this.setState({"UserData": value});
            // })

            const value = await AsyncStorage.getItem('UserData');
            console.log("value to retrive data ==>> " + JSON.stringify(value));
            object = JSON.parse(value);
            // array = Object.keys(object).map(function (data) {
            //     return object[data];
            // });
            console.log("Object==>> " + JSON.stringify(object))
            console.log("object.dev_access_token==>> " + object.dev_access_token)
            if (object !== null) {
                // console.log("Value==>> " + value)
                this.setState({
                    device_Access_Token: object.dev_access_token,
                    UName: object.user_name,
                    UPhoto: object.user_photo
                })
                console.log("Device_Access_token==>> " + this.state.device_Access_Token);
            }
            // console.log("UserData to retrive data ==>> " + JSON.stringify(UserData))
            //     .then(value => {
            //         if (value !== null) {
            //             console.log("Value==>> " + value);
            //             this.setState({
            //                 device_Access_Token: value.dev_access_token,
            //                 UName: value.user_name,
            //                 UPhoto: value.user_photo
            //             })
            //         }
            //     })

        } catch (error) {
            console.log("error==> " + error);
        }
    };

    getLoginOnPress() {
        const { TermOfUse, } = this.state;
        return (
            <View>
                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Login', {
                        TermOfUseData: TermOfUse,
                    })}
                    style={{ width: 120, height: 35, borderRadius: 10, backgroundColor: '#009933', justifyContent: 'center' }}
                >
                    <Text style={{ alignSelf: 'center', color: '#ffffff' }}> Login </Text>
                </TouchableOpacity>
            </View>
        )
    }

    getLogoutOnPress() {
        const { navigation, goBack, } = this.props;
        // const key = navigation.getParam('LoginDetail', 'NO-Symbol');
        // console.log("Key==>>" + JSON.stringify(key))
        return (
            <View style={{ height: 100, flexDirection: 'row', }}>
                <View style={{ height: 100, width: "70%", flexDirection: 'column' }}>
                    <View>
                        <Image
                            source={{ uri: this.state.UPhoto }}
                            style={{
                                marginLeft: 10,
                                borderWidth: 2,
                                height: 65,
                                width: 65,
                                borderRadius: 50,
                                padding: 5,
                            }}
                        />
                    </View>
                    <View style={{
                        // elevation: 1,
                        // backgroundColor: 'rgba(0,0,0,0.2)'
                    }}>
                        <Text
                            style={{
                                fontSize: 23,
                                color: '#000000',
                                fontWeight: 'bold',
                                margin: 5,
                            }}>
                            {this.state.UName}
                        </Text>
                    </View>
                </View>

                <View style={{
                    justifyContent: 'center',
                    width: "30%",
                    // elevation: 1,
                    // backgroundColor: 'rgbargba(0,0,0,0.2)',
                }}>
                    <TouchableOpacity
                        onPress={this.onLogout}
                        style={{
                            width: 100,
                            height: 35,
                            borderRadius: 10,
                            backgroundColor: '#009933',
                            justifyContent: 'center',
                        }}
                    >
                        <Text
                            style={{
                                alignSelf: 'center',
                                color: '#ffffff',
                            }}>
                            Log out
                        </Text>
                    </TouchableOpacity>
                </View>
            </View >
        )
    }

    onLogout = () => {
        // const { navigation, goBack, } = this.props;
        // const key = navigation.getParam('LoginDetail', 'NO-Symbol');
        // console.log("Array logout Data ==>> " + JSON.stringify(key))
        console.log("this.state.device_Access_Token==>> " + this.state.device_Access_Token)
        // const url = 'https://api.cointopper.com/api/v3/logout'
        fetch('https://api.cointopper.com/api/v3/logout', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                device_access_token: this.state.device_Access_Token
            })
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log("Logout Response==>> " + JSON.stringify(responseJson))

                if (responseJson.status === 1) {
                    // AsyncStorage.removeItem("UserData");
                    this.removeValue();
                    this.setState({
                        device_Access_Token: "",
                        UName: "",
                        UPhoto: "",
                    })
                    console.log("logout ")
                }
            })
            .catch((error) => {
                // console.log(error)
            })
    }

    removeValue = async () => {
        try {
            await AsyncStorage.removeItem('UserData')
        } catch (e) {
            // remove error
        }
        console.log('Done.')
    }

    render() {
        const { TermOfUse, PrivacyPolicy, selectedStringData, device_Access_Token } = this.state;
        const { navigate } = this.props.navigation;
        const { navigation, goBack, } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ flex: 1, flexDirection: 'column', }}>

                        {/* Header Container */}
                        <LinearGradient
                            colors={['#009933', '#00cc00']}
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 0 }}
                        >
                            <View style={{
                                height: 80,
                                alignItems: 'flex-start',
                                flexDirection: 'column',
                                padding: 10
                            }}>

                                {/* Lable Component */}
                                <View style={{ flex: 1, alignItems: 'flex-start', flexDirection: 'row' }}>
                                    <View style={{ flexDirection: 'column', alignItems: 'stretch' }}>
                                        <Text style={{ fontSize: 17, fontStyle: 'normal', color: '#f2f2f2' }}>
                                            Manage Accounts and More
                                    </Text>
                                        <Text style={{
                                            fontSize: SCREEN_WIDTH * 0.09,
                                            fontWeight: 'bold',
                                            fontStyle: 'normal',
                                            color: '#ffffff'
                                        }}>
                                            Profile
                                    </Text>
                                    </View>
                                </View>
                            </View>
                        </LinearGradient>

                        {/* Login Button  */}
                        <View style={{ margin: 4, paddingLeft: 10 }}>
                            {device_Access_Token === "" ? this.getLoginOnPress() : this.getLogoutOnPress()}
                            <NavigationEvents onDidFocus={() => this._retrieveData()} />
                        </View>

                        {/* Manage Notification */}
                        <View style={{ margin: 4 }}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('ManageNotification')}
                                style={{ width: "100%", height: 40, borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#000000', backgroundColor: '#ffffff', justifyContent: 'center' }}
                            >
                                <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                                    <View style={{ alignSelf: 'flex-start', width: "90%" }}>
                                        <Text style={{ alignSelf: 'flex-start', color: '#000000', fontSize: 16 }}> Manage Notifications </Text>
                                    </View>
                                    <View style={{ alignSelf: 'flex-end', width: "10%" }}>
                                        <Icon
                                            name='right'
                                            style={{ alignSelf: 'flex-end' }}
                                            size={20}
                                            color='rgba(0, 0, 0, 0.8)'
                                            backgroundColor='#f2f2f2'
                                        />
                                    </View>
                                </View>
                            </TouchableOpacity>
                        </View>

                        {/* Modal for Term Of Use And Privacy Policy */}
                        <View>
                            <Modal
                                animationType="fade"
                                transparent={true}
                                visible={this.state.modalVisible}
                                onRequestClose={this.closeModal}
                            >
                                <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.5)' }}>
                                    <View style={{
                                        height: "90%",
                                        width: "90%",
                                        alignSelf: 'center',
                                        backgroundColor: "white",
                                        padding: 10,
                                        marginTop: 20,
                                    }}>
                                        <ScrollView
                                            style={{ height: SCREEN_WIDTH * 0.7, padding: 10 }}
                                            contentContainerStyle={{ backgroundColor: 'white', flexGrow: 1, }}
                                            onContentSizeChange={this.onContentSizeChange}
                                        >
                                            <View style={{ padding: 10, alignSelf: "center" }}>
                                                <AutoHeightWebView
                                                    style={{ width: Dimensions.get('window').width - 100, padding: 10 }}
                                                    customScript={`document.body.style.background = 'white';`}
                                                    customStyle={`
                                                                    * {
                                                                        font-family: 'Times New Roman';
                                                                    }
                                                                    p {
                                                                        font-size: 16px;
                                                                    }
                                                                `}
                                                    onSizeUpdated={size => console.log(size.height)}
                                                    files={[{
                                                        href: 'cssfileaddress',
                                                        type: 'text/css',
                                                        rel: 'stylesheet'
                                                    }]}
                                                    source={{ html: selectedStringData }}
                                                    zoomable={false}
                                                />
                                            </View>
                                        </ScrollView>
                                        {/* Button Cancel And Save */}
                                        <View style={{ paddingRight: 20, flexDirection: "row", justifyContent: "space-between" }}>
                                            <View style={{ flex: 1, padding: 10 }}>
                                                <TouchableOpacity
                                                    style={{ alignSelf: 'center' }}
                                                    onPress={() => this.closeModal(false)}
                                                >
                                                    <Text style={{ color: "#0000ff", fontSize: 15 }}>Cancel</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </Modal>
                        </View>

                        {/* Term Of Use */}
                        <View style={{ margin: 4, }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.PPAndTermOfUse(TermOfUse.text);
                                }}
                                style={{ width: "100%", height: 30, paddingLeft: 5, backgroundColor: '#ffffff', justifyContent: 'center' }}
                            >
                                <Text style={{ alignSelf: 'flex-start', color: '#000000', fontSize: 16 }}> Terms of use </Text>
                            </TouchableOpacity>
                        </View>

                        {/* privacypolicy */}
                        <View style={{ margin: 4, }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.PPAndTermOfUse(PrivacyPolicy.text);
                                }}
                                style={{ width: "100%", height: 30, paddingLeft: 5, backgroundColor: '#ffffff', justifyContent: 'center' }}
                            >
                                <Text style={{ alignSelf: 'flex-start', color: '#000000', fontSize: 16 }}> Privacy Policy </Text>
                            </TouchableOpacity>
                        </View>

                        {/* Visit CoinTopper */}
                        <View style={{ margin: 4 }}>
                            <TouchableOpacity
                                onPress={() => Linking.openURL('https://cointopper.com')}
                                style={{ width: "100%", height: 30, paddingLeft: 5, backgroundColor: '#ffffff', justifyContent: 'center' }}
                            >
                                <Text style={{ alignSelf: 'flex-start', color: '#000000', fontSize: 16 }}> Visit CoinTopper </Text>
                            </TouchableOpacity>
                        </View>

                        {/* Rate Us */}
                        <View style={{ margin: 4 }}>
                            <TouchableOpacity
                                onPress={() => console.log("hiiii")}
                                style={{ width: "100%", height: 30, paddingLeft: 5, backgroundColor: '#ffffff', justifyContent: 'center' }}
                            >
                                <Text style={{ alignSelf: 'flex-start', color: '#000000', fontSize: 16 }}> Rate Us </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </SafeAreaView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    loader: {
        marginTop: 10,
        alignItems: "center"
    },
    buttonStyle: {
        borderRadius: 12,
        shadowOpacity: 10,
        padding: 10,
        backgroundColor: '#ff4d94',
    },
})