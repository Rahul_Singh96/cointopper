import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    StyleSheet,
    SafeAreaView,
    ScrollView,
    Image,
    RefreshControl,
    TouchableOpacity,
    Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import LinearGradient from 'react-native-linear-gradient';
const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
} = Dimensions.get('window')

export default class TopViewed extends Component {
    constructor() {
        super();
        this.state = {
            isLoading: true,
            dataSource: [],
            refreshing: false,

        }
    }
    _onRefresh = () => {
        const url = 'https://api.cointopper.com/api/v3/topsearched'
        this.setState({
            dataSource: [],
            refreshing: true
        })
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log("Refresh Data:-" + responseJson.data);
                this.setState({
                    dataSource: responseJson.data,
                    refreshing: false
                })
            })
            .catch((error) => {
                console.log(error)
            })
    }

    componentDidMount() {
        const url = 'https://api.cointopper.com/api/v3/topsearched'
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    dataSource: responseJson.data
                })
            })
            .catch((error) => {
                console.log(error)
            })
    }

    nFormatter(market_cap_usd) {
        if (market_cap_usd >= 1000000000) {
            return (market_cap_usd / 1000000000).toFixed(2) + 'B';
        }
        if (market_cap_usd >= 1000000) {
            return (market_cap_usd / 1000000).toFixed(2) + 'M';
        }
        if (market_cap_usd >= 1000) {
            return (market_cap_usd / 1000).toFixed(2).replace(/\.0$/, '') + 'K';
        }
        return market_cap_usd;
    }

    _goback() {
        const { goBack } = this.props.navigation;
        goBack();
        // setTimeout(function(){ goBack() }, 5000);
    }

    renderItem = ({ item }) => {
        return (
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'row',
                    borderBottomWidth: 0.5,
                    padding: 5,
                    borderBottomColor: 'grey',
                }}>
                <View style={{ flex: 3, alignItems: 'flex-start', justifyContent: 'flex-start', flexDirection: 'row' }}>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <Image
                            source={{ uri: item.logo }}
                            style={{ width: 30, height: 30, borderRadius: 37.5 }}
                        />
                    </View>

                    <View key={item.name} style={{ flex: 3, flexDirection: "column", alignItems: 'flex-start', justifyContent: 'flex-start', }}>
                        <Text style={{ color: '#001a33' }}>{item.name}</Text>
                        <View key={item.market_cap_usd} style={{ flex: 3, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start', }}>
                            <Text style={{ color: '#a6a6a6' }}>{item.symbol} / {this.nFormatter(item.market_cap_usd)}</Text>
                        </View>
                    </View>
                </View>
                <View key={item.percent_change24h} style={{ flex: 2, alignItems: 'center', justifyContent: 'center', }}>
                    <Text style={{ color: '#001a33' }}>{item.percent_change24h.toFixed(2)} %</Text>
                </View>
                <View key={item.price} style={{ flex: 2, justifyContent: 'flex-start', alignItems: 'flex-start', flexDirection: 'column' }}>
                    <Text style={{ color: '#005580' }}>$ {item.price.toFixed(2)}</Text>
                    <Text style={{ color: '#a6a6a6' }}>B {item.price_btc.toFixed(8)}</Text>
                </View>
            </View>
        );
    }
    render() {
        const { dataSource } = this.state;
        const { goBack } = this.props.navigation;
        return (
            <View style={{ flex: 1, }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <ScrollView
                        contentInsetAdjustmentBehavior="automatic"
                        style={styles.scrollView}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh.bind(this)}
                            />
                        }
                    >
                        <View style={{ flex: 1, flexDirection: 'column', }}>

                            <View style={{ height: 100, alignItems: 'flex-start', flexDirection: 'column' }}>
                                <LinearGradient
                                    colors={['#ff0066', '#ff0000']}
                                    style={{ width: "100%", height: "100%" }}
                                    start={{ x: 1, y: 0 }}
                                    end={{ x: 0, y: 1 }}
                                >
                                    <View style={{ flex: 1, alignItems: 'flex-start', flexDirection: 'row' }}>
                                        <View style={{ flexDirection: 'column', alignItems: 'stretch', padding: 5 }}>
                                            <Text style={{ fontSize: 17, fontStyle: 'normal', color: '#f2f2f2' }}>
                                                Welcome to CoinTopper
                                    </Text>
                                            <Text style={{ fontSize: SCREEN_WIDTH * 0.09, fontWeight: 'bold', fontStyle: 'normal', color: '#ffffff' }}>
                                                Top Viewed Coins
                                    </Text>
                                        </View>

                                        <View style={{ flex: 1, alignItems: 'flex-end', padding: 10 }}>
                                            < TouchableOpacity
                                                onPress={() => this._goback()}
                                            >
                                                <Icon
                                                    name='closecircleo'
                                                    size={35}
                                                    color='#ffffff'
                                                    backgroundColor='#f2f2f2'
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </LinearGradient>
                            </View>

                            <View style={{
                                flex: 1,
                                // height: 10,
                                // alignItems: 'center',
                                // justifyContent: 'center',
                                flexDirection: 'column',
                                // padding: 10,
                            }}>
                                <View style={{ height: 40, flexDirection: 'row', padding: 10 }}>
                                    <View style={{ flex: 1, alignItems: 'flex-start', flexDirection: 'row' }}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={{ fontSize: 13, color: '#005580' }} onPress={() => this.sortList('name')}>NAME /</Text>
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            <Text style={{ fontSize: 13, color: '#005580' }} onPress={() => this.sortList('market_cap_usd')}>   M.CAP</Text>
                                        </View>
                                    </View>

                                    <View style={{ flex: 1, alignItems: 'center', }}>
                                        <Text style={{ fontSize: 13, color: '#005580' }} onPress={() => this.sortList('percent_change24h')}>CHANGE</Text>
                                    </View>

                                    <View style={{ flex: 1, alignItems: 'center', }}>
                                        <Text style={{ fontSize: 13, color: '#005580' }} onPress={() => this.sortList('price')}>PRICE</Text>
                                    </View>
                                </View>
                                <View style={{ flex: 1, flexDirection: "column" }}>
                                    <FlatList
                                        data={this.state.dataSource}
                                        renderItem={this.renderItem}
                                        keyExtractor={(item, index) => index}
                                    />
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: 'white',
    },
    NameBarStyle: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        margin: 5,
    },
})

