import React, { Component } from 'react';
import {
    SafeAreaView,
    ScrollView,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    FlatList,
    Image,
    Dimensions,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/AntDesign';
import { SearchBar, ListItem } from 'react-native-elements';
const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
} = Dimensions.get('window')
class SearchComp extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: true,
            dataSource: [],
            text: "",
            itemData: [],
            searchedData: [],
        }

    }

    componentDidMount() {
        const url = 'https://api.cointopper.com/api/v3/search'
        fetch(url)
            .then(response => response.json())
            .then((responseJson) => {
                console.log("Data Load")
                this.setState({
                    dataSource: responseJson.data,
                    isLoading: false,
                });
            })
            .catch((error) => {
                console.log(error);
            });
    };

    searchFilterFunction = text => {
        this.setState({
            value: text,
        });
        const newArryItems = [];
        if (text == "") {

        } else {
            this.state.dataSource.filter(item => {
                const itemData = `${item.id}`;
                const itemLower = itemData.toLowerCase();
                const textData = text.toLowerCase();
                if (itemLower.indexOf(textData) > -1) {
                    newArryItems.push(item);
                }
            });
        }
        // console.log("newArryItems =>>" + newArryItems);
        this.setState({
            searchedData: newArryItems,
        });
    };

    _goback() {
        const { goBack } = this.props.navigation;
        goBack();
        // setTimeout(function(){ goBack() }, 5000);
    }

    renderItem = ({ item }) => {
        return (
            <View
                style={{
                    flex: 1,
                    flexDirection: 'row',
                    borderBottomWidth: 0.5,
                    padding: 5,
                    borderBottomColor: 'grey',
                }}>
                <View key={item.logo} style={{ flex: 1, alignItems: 'flex-start', padding: 5 }}>
                    <Image
                        source={{ uri: item.logo }}
                        style={{ width: 30, height: 30, borderRadius: 37.5 }}
                    />
                </View>
                <View key={item.value} style={{ flex: 5, alignItems: 'flex-start', padding: 5 }}>
                    <Text style={{ fontSize: 20, fontWeight: '300' }}>{item.value}</Text>
                </View>
            </View>
        )
    }

    render() {
        const { searchedData } = this.state;
        const { goBack } = this.props.navigation;
        return (
            <View style={{ flex: 1, }}>
                <SafeAreaView>
                    <ScrollView
                        contentInsetAdjustmentBehavior="automatic"
                        style={styles.scrollView}
                    >
                        <LinearGradient
                            colors={['#003399', '#0073e6']}
                            // style={{ width: "100%", height: "100%", borderRadius: 15 }}
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 0 }}
                        >
                            <View style={{ fontSize: SCREEN_HEIGHT * 0.09, alignItems: 'flex-start', flexDirection: 'row', padding:10}}>
                                <View style={{ width:"90%", paddingBottom:10 }}>
                                    <SearchBar
                                        placeholder='Search Coin'
                                        round
                                        placeholderTextColor={'#ffffff'}
                                        inputStyle={{
                                            alignSelf: 'stretch',
                                            minWidth: "100%",
                                            padding: 10
                                        }}
                                        containerStyle={{
                                            backgroundColor: 'transparent',
                                            width: '100%',
                                            height: 40,
                                            borderWidth: 1,
                                            borderRadius: 0.1,
                                            borderColor: 'transparent'
                                        }}
                                        onChangeText={text => this.searchFilterFunction(text)}
                                        value={this.state.value}
                                        autoCorrect={false}
                                    />
                                </View>
                                <View style={{ width:"10%", paddingTop:13 }}>
                                    < TouchableOpacity
                                        onPress={() => this._goback()}
                                    >
                                        <Icon
                                            name='closecircleo'
                                            size={30}
                                            color='#ffffff'
                                            backgroundColor='#f2f2f2'
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </LinearGradient>
                        <View style={{ flex: 4, flexDirection: 'column', }}>
                            <FlatList
                                data={searchedData}
                                keyExtractor={(item, index) => index}
                                renderItem={({ item }) => (
                                    <ListItem
                                        style={{ borderBottomWidth: 0.4 }}
                                        leftAvatar={{ source: { uri: item.logo } }}
                                        title={`${item.id} ${item.value}`}
                                    />
                                )}
                            />
                        </View>

                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: "white",
    },
    TextInputStyle: {
        height: 40,
        width: "100%",
        borderRadius: 10,
        backgroundColor: "#2e5cb8",
        color: '#ffffff',
        marginRight: 5,
    }
});

export default SearchComp;