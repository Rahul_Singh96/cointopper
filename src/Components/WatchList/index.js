import React, { Component} from 'react';
import { View, Text } from 'react-native';
import { createMaterialTopTabNavigator, createStackNavigator } from 'react-navigation';
import CoinWatchList from './CoinWatchList';
import ICOWatchList from './ICOWatchList';
import AirDropWatchList from './AirDropWatchList';

const WatchList = createMaterialTopTabNavigator(
    {
        coinwatchlist : {
            screen : CoinWatchList,
        },
        icowatchlist : {
            screen: ICOWatchList,
        },
        airdropwatchlist : {
            screen : AirDropWatchList,
        },
    },
    {
        initialRouteName: "coinwatchlist",
        tabBarPosition: 'top',
        swipeEnabled: true,
        animationEnabled: true,
        tabBarOptions: {
          activeTintColor: '#FFFFFF',
          inactiveTintColor: '#F8F8F8',
          pressColor :"black",
          style: {
            backgroundColor: 'white',
          },
          labelStyle: {
            textAlign:"center",
            borderRadius:10,
            height:40,
            borderWidth:1,
            borderColor:"blue",
            fontSize:11,
            backgroundColor:"blue",
          },
          tabStyle: {
            width: "100%",
          },
          indicatorStyle: {
            borderBottomColor: '#87B56A',
            borderBottomWidth: 2,
          },
        },
      },
);

export default WatchList;