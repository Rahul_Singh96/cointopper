import React, { Component } from 'react';
import {
    View,
    Text,
    SafeAreaView,
    StyleSheet,
    TouchableOpacity,
    Image,
    Dimensions,
    Modal,
    Picker,
    TextInput,
} from 'react-native';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import Icon from 'react-native-vector-icons/AntDesign';
import LinearGradient from 'react-native-linear-gradient';
import RadioButton from './AlertComponent/RadioButton';
import DeviceInfo from 'react-native-device-info';

const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
} = Dimensions.get('window')



export default class Alerts extends Component {
    constructor() {
        super();
        this.state = {
            modalVisible: false,
            picked: "",
            visibleFilter: false,
            Currency: "USD",
            isLoading: true,
            dataSource: [],
            text: "",
            ModalArray: [],
            DetailAlert: [],
            deviceId: '',
            coinId: "",
            Usd_Price: "",
            Btc_Price: "",
            is_Repeat: "",
            Device_Token:"",
        }
    };

    componentDidMount() {
        const url = 'https://api.cointopper.com/api/v3/ticker'
        fetch(url)
            .then(response => response.json())
            .then((responseJson) => {
                let getArray = responseJson.data;
                // console.log("getArray:-" + JSON.stringify(getArray));
                // console.log("getArray Length:-" + getArray.length)
                let tempArray = [];
                if (getArray.length > 0) {
                    for (let item of getArray) {
                        // console.log("inside loop")
                        tempArray.push({
                            key: item.name,
                            label: item.name,
                        })
                    };
                    // console.log("tempArray:-" + JSON.stringify(tempArray));
                    this.setState({
                        ModalArray: tempArray,
                        DetailAlert: [...this.state.DetailAlert, responseJson.data[0]],
                        picked: responseJson.data[0].name,
                        dataSource: responseJson.data,
                        isLoading: false,
                    });
                }
            })
            .catch((error) => {
                console.log(error);
            });
    };

    IsRepeatFunc= () =>{
        this.CreateNewAlert();
    }

    CreateNewAlert() {
        var url = 'https://api.cointopper.com/api/v3/newalert';
        fetch(url, {
            method: 'POST',
            body: JSON.stringify({ "device_access_token": this.state.Device_Token, "coinid": this.state.coinId, "price_usd": this.state.Usd_Price, "price_btc": this.state.Btc_Price, "is_repeat": this.state.is_Repeat })
        }).then(function (response) {
            return response.json();
        }).then(function (Currency) {
            if(is_Repeat == 1){
            if (this.state.Currency == "USD") {
                that.setState({ 
                    Device_Token:this.state.deviceId,
                    coinId:this.state.DetailAlert[0].id,
                    Usd_Price:this.state.DetailAlert[0].price,
                    Btc_Price:"",
                    is_Repeat:1,
                });
            } else {
                that.setState({ 
                    device_access_token:this.state.deviceId,
                    coinId:this.state.DetailAlert[0].id,
                    Usd_Price:"",
                    Btc_Price:this.state.DetailAlert[0].price_btc,
                    is_Repeat:1,
                });
            }
        }else{
            if (this.state.Currency == "USD") {
                that.setState({ 
                    Device_Token:this.state.deviceId,
                    coinId:this.state.DetailAlert[0].id,
                    Usd_Price:this.state.DetailAlert[0].price,
                    Btc_Price:"",
                    is_Repeat:2,
                });
            } else {
                that.setState({ 
                    device_access_token:this.state.deviceId,
                    coinId:this.state.DetailAlert[0].id,
                    Usd_Price:"",
                    Btc_Price:this.state.DetailAlert[0].price_btc,
                    is_Repeat:2,
                });
            }
        }
        }).catch(function (error) {
            console.log(error);
        });
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    closeModal(visible) {
        this.setState({ modalVisible: visible });
    }

    _goback() {
        const { goBack } = this.props.navigation;
        goBack();
    }
    // Getting the Unique Id from here
    getDeviceId = () => {
        var id = DeviceInfo.getUniqueID();
        this.setState({ deviceId: id, });
    };

    render() {
        const { ModalArray } = this.state;
        // console.log("dataSource =>", dataSource);
        // console.log("main", JSON.stringify(ModalArray));
        // console.log("Detail ALert Data:")
        // console.log(this.state.DetailAlert[0] === undefined ? "" : JSON.stringify(this.state.DetailAlert[0]));
        const { goBack } = this.props.navigation;
        const options = [
            {
                key: '1',
                text: 'One Time',
            },
            {
                key: '2',
                text: 'Repeating',
            },
        ];
        console.log("Divide Unique ID ==> " + this.state.deviceId);
        return (
            <View style={{ flex: 1, }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ flex: 1, flexDirection: 'column', }}>
                        {/* header Content */}
                        <LinearGradient
                            colors={['#ff0066', '#ff0000']}
                            // style={{ width: "100%", height: "100%" }}
                            start={{ x: 1, y: 0 }}
                            end={{ x: 0, y: 1 }}
                        >
                            <View style={{ height: 80, alignItems: 'flex-start', flexDirection: 'row', }}>
                                <View style={{ flex: 1, alignItems: 'flex-start', flexDirection: 'column', padding: 5 }} >
                                    <Text style={{ fontSize: 17, fontStyle: 'normal', color: '#f2f2f2' }}>
                                        Your Alerts List
                                    </Text>
                                    <Text style={{ fontSize: SCREEN_WIDTH * 0.09, fontWeight: 'bold', fontStyle: 'normal', color: '#ffffff' }}>
                                        ALerts
                                    </Text>
                                </View>
                                <View style={{ flex: 1, alignItems: 'flex-end', padding: 10 }}>
                                    < TouchableOpacity
                                        onPress={() => this._goback()}
                                    >
                                        <Icon
                                            name='closecircleo'
                                            size={35}
                                            color='#ffffff'
                                            backgroundColor='#f2f2f2'
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </LinearGradient>

                        {/* Screen Content */}
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', backgroundColor: '#fffff', }}>
                            <View style={{ flex: 1, alignItems: 'center' }}>
                                <Image
                                    source={require('../Images/stopwatch-64.png')}
                                />
                                <Text>
                                    No Alerts added yet
                                </Text>
                                <Text>
                                    use (+) sign to add your first Alert
                                </Text>
                            </View>
                        </View>
                        {/* Add Alert Content using TouchableOpacity */}
                        <View style={{
                            backgroundColor: "grey",
                            position: "absolute",
                            borderRadius: 100,
                            width: 50,
                            height: 50,
                            right: 10,
                            bottom: 10,
                            zIndex: 1,
                        }}>
                            <Modal
                                animationType="fade"
                                transparent={true}
                                visible={this.state.modalVisible}
                                onRequestClose={() => {
                                    Alert.alert('Modal has been closed.');
                                }}
                            >
                                <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.5)' }}>
                                    <TouchableOpacity
                                        style={{ height: 85, width: "100%" }}
                                        onPress={() => this.closeModal(false)}
                                    >
                                        <Text>  </Text>
                                    </TouchableOpacity>

                                    <View style={{
                                        // margin: 90,
                                        height: "65%",
                                        width: "90%",
                                        alignSelf: 'center',
                                        backgroundColor: "white",
                                        padding: 10,
                                    }}>

                                        {/* header Add New Alert  */}
                                        <View style={{ height: 40, flexDirection: "row", alignSelf: "center", padding: 10 }}>
                                            <View style={{ width: "90%", alignItems: "center" }}>
                                                <Text style={{ fontSize: 20, fontWeight: "bold", color: "#e60000" }}> Add New Alert </Text>
                                            </View>
                                            <View style={{ width: "10%", alignSelf: "center" }}>
                                                < TouchableOpacity
                                                    onPress={() => this.closeModal(false)}
                                                >
                                                    <Icon
                                                        name='closecircleo'
                                                        size={25}
                                                        color='red'
                                                        backgroundColor='red'
                                                    />
                                                </TouchableOpacity>
                                            </View>
                                        </View>

                                        {/* set Coin */}
                                        <View style={{ paddingRight: 20 }}>
                                            <View style={{ paddingLeft: 10 }}>
                                                <Text style={{ color: "#0066ff" }}> Set Coin </Text>
                                            </View>
                                            <View style={{ padding: 10 }}>
                                                <TouchableOpacity style={styles.PickerStyle} onPress={this.onShow}>

                                                    <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
                                                        <View style={{ width: "20%", alignItems: 'flex-start', justifyContent: "space-around" }}>
                                                            <Image
                                                                source={{ uri: this.state.DetailAlert[0] === undefined ? null : this.state.DetailAlert[0].logo }}
                                                                style={{ width: 30, height: 30, borderRadius: 37.5 }}
                                                            />
                                                        </View>
                                                        <View style={{ width: "70%", alignItems: 'flex-start', }}>
                                                            <Text>{this.state.DetailAlert[0] === undefined ? "" : this.state.DetailAlert[0].name}  {this.state.DetailAlert[0] === undefined ? "" : `(${this.state.DetailAlert[0].symbol})`}</Text>
                                                        </View>
                                                    </View>

                                                </TouchableOpacity>
                                                <ModalFilterPicker
                                                    showFilter={true}
                                                    visible={this.state.visibleFilter}
                                                    onSelect={this.onSelect}
                                                    onCancel={this.onCancel}
                                                    options={ModalArray}
                                                />
                                            </View>
                                        </View>

                                        {/* Current Price */}
                                        <View style={{ paddingRight: 20 }}>
                                            <View style={{ paddingLeft: 10 }}>
                                                <Text style={{ color: "#0066ff" }}> Current Price </Text>
                                            </View>
                                            <View style={{ flexDirection: "row", justifyContent: "space-between", padding: 10 }}>
                                                <View style={{ flex: 1 }}>
                                                    <Text > USD: ${this.state.DetailAlert[0] === undefined ? "" : this.state.DetailAlert[0].price.toFixed(3)} </Text>
                                                </View>
                                                <View style={{ flex: 1, }}>
                                                    <Text style={{ color: "#ff9900", }}>BTC: B{this.state.DetailAlert[0] === undefined ? "" : this.state.DetailAlert[0].price_btc.toFixed(6)} </Text>
                                                </View>
                                            </View>
                                        </View>

                                        {/* Set Price */}
                                        <View style={{ paddingRight: 20 }}>
                                            <View style={{ paddingLeft: 10 }}>
                                                <Text style={{ color: "#0066ff" }}> Set Price </Text>
                                            </View>
                                            <View style={{ flexDirection: "row", padding: 10, justifyContent: "space-evenly" }}>
                                                <View style={{ width: "65%" }}>
                                                    <TextInput
                                                        style={styles.input}
                                                        placeholder='Price in BTC/USD '
                                                        keyboardType={"numeric"}
                                                    >
                                                        <Text style={{ fontSize: 16 }}>  {this.state.Currency == "USD" ? this.state.DetailAlert[0] === undefined ? "" : this.state.DetailAlert[0].price.toFixed(3) : this.state.DetailAlert[0] === undefined ? "" : this.state.DetailAlert[0].price_btc.toFixed(6)}</Text>
                                                    </TextInput>
                                                </View>
                                                <View style={{ width: "25%" }}>
                                                    <Picker
                                                        mode="dialog"
                                                        selectedValue={this.state.Currency}
                                                        style={{ height: 40, width: 100, backgroundColor: "#f2f2f2", borderRadius: 10, }}
                                                        onValueChange={(itemValue, itemIndex) =>
                                                            this.setState({
                                                                Currency: itemValue
                                                            })
                                                        }>
                                                        <Picker.Item label="USD" value="USD" />
                                                        <Picker.Item label="BTC" value="BTC" />
                                                    </Picker>
                                                </View>
                                            </View>
                                        </View>

                                        {/* Repeat  */}
                                        <View style={{ paddingRight: 20 }}>
                                            <View style={{ paddingLeft: 10 }}>
                                                <Text style={{ color: "#0066ff" }}> Repeat </Text>
                                            </View>
                                            <View style={{ padding: 10 }}>
                                                <View style={{ width: "80%", }}>
                                                    <RadioButton options={options} />
                                                </View>
                                            </View>
                                        </View>

                                        {/* Button Cancel And Save */}
                                        <View style={{ paddingRight: 20, flexDirection: "row", justifyContent: "space-between" }}>
                                            <View style={{ flex: 1, padding: 10 }}>
                                                <TouchableOpacity
                                                    style={styles.AlertButtonStyle}
                                                    onPress={() => this.closeModal(false)}
                                                >
                                                    <Text style={{ color: "#ff0000" }}>Cancel</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{ flex: 1, padding: 10 }}>
                                                <TouchableOpacity
                                                    style={styles.AlertButtonStyle}
                                                    onPress={this.IsRepeatFunc()}
                                                >

                                                    <Text style={{ color: "#ff0000" }}>Save</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                    <TouchableOpacity
                                        style={{ height: 200, width: "100%", }}
                                        onPress={() => this.closeModal(false)}
                                    >
                                    </TouchableOpacity>
                                </View>
                            </Modal>
                            <TouchableOpacity
                                style={styles.buttonStyle}
                                onPress={() => {
                                    this.setModalVisible(true);
                                }}
                            >
                                <Icon
                                    name='plus'
                                    size={30}
                                    color='#ffffff'
                                    backgroundColor='#f2f2f2'
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </SafeAreaView>
            </View>
        );
    }

    onShow = () => {
        this.setState({ visibleFilter: true });
    }

    onSelect = (picked) => {
        for (let i = 0; i <= this.state.dataSource.length - 1; i++) {
            // console.log("mainnnnnnnnn", this.state.dataSource[i].symbol);
            if (picked == this.state.dataSource[i].name) {
                // console.log("Inside IF:-" + JSON.stringify(this.state.dataSource[i]));
                this.setState({
                    DetailAlert: [this.state.dataSource[i]],
                    picked: picked,
                    visibleFilter: false,
                })
                // console.log("ASdsdasdasdasdasd", picked);
            }
        }
        // console.log("ASdsdasdasdasdasd",picked);
    }

    onCancel = () => {
        this.setState({
            visibleFilter: false
        });
    }
}

const styles = StyleSheet.create({
    input: {
        height: 40,
        borderRadius: 12,
        backgroundColor: "#f2f2f2",
    },
    buttonStyle: {
        width: 50,
        borderRadius: 100,
        shadowOpacity: 10,
        height: 50,
        padding: 10,
        backgroundColor: 'red',
        alignContent: 'center'
    },
    PickerStyle: {
        width: "100%",
        height: 40,
        color: '#ffffff',
        alignItems: "center",
        justifyContent: 'center',
        backgroundColor: '#f2f2f2',
        borderRadius: 12,
        shadowOpacity: 10,
    },
    AlertButtonStyle: {
        padding: 10,
        alignItems: "center",
        height: 40,
        backfaceVisibility: "hidden",
        borderRadius: 12,
        // borderWidth:2,
        // borderColor:"black",
    },
})