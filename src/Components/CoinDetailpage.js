import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    FlatList,
    StyleSheet,
    SafeAreaView,
    Image,
    ImageBackground,
    TouchableOpacity,
    Dimensions,
    WebView,
    Linking,
} from "react-native";
import Icon from 'react-native-vector-icons/Entypo';
import SIcon from 'react-native-vector-icons/SimpleLineIcons'
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import LinearGradient from 'react-native-linear-gradient';
const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
} = Dimensions.get('window');

export default class CoinDetailpage extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: true,
            CoinDetails: [],
            screenHeight: SCREEN_HEIGHT,
            ApiLink: "",
            // ImpLinks: [],
        }
    };

    componentDidMount() {
        this.getListCoin();
    };

    getListCoin = () => {
        const { navigation } = this.props;
        const ApiLink = 'https://api.cointopper.com/api/v3/ticker/';
        const key = navigation.getParam('CoinArray', 'NO-Symbol');
        const url = ApiLink + key.symbol;
        // console.log("URL=> " + url);
        // console.log("key" + key);
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    CoinDetails: responseJson.data,

                })
            })
            .catch((error) => {
                console.log(error)
            })
    };

    _goback() {
        const { goBack } = this.props.navigation;
        goBack();
        // setTimeout(function(){ goBack() }, 5000);
    }

    onContentSizeChange = (contentWidth, contentHeight) => {
        this.setState({ screenHeight: contentHeight });
    };

    nFormatMarket(market_cap_usd) {
        if (market_cap_usd >= 1000000000) {
            return (market_cap_usd / 1000000000).toFixed(2) + 'B';
        }
        if (market_cap_usd >= 1000000) {
            return (market_cap_usd / 1000000).toFixed(2) + 'M';
        }
        if (market_cap_usd >= 1000) {
            return (market_cap_usd / 1000).toFixed(2).replace(/\.0$/, '') + 'K';
        }
        return market_cap_usd;
    };

    nFormatVolume(volume24h_usd) {
        if (volume24h_usd >= 1000000000) {
            return (volume24h_usd / 1000000000).toFixed(2) + 'B';
        }
        if (volume24h_usd >= 1000000) {
            return (volume24h_usd / 1000000).toFixed(2) + 'M';
        }
        if (volume24h_usd >= 1000) {
            return (volume24h_usd / 1000).toFixed(2).replace(/\.0$/, '') + 'K';
        }
        return volume24h_usd;
    };

    nFormatAvail(available_supply) {
        if (available_supply >= 1000000000) {
            return (available_supply / 1000000000).toFixed(2) + 'B';
        }
        if (available_supply >= 1000000) {
            return (available_supply / 1000000).toFixed(2) + 'M';
        }
        if (available_supply >= 1000) {
            return (available_supply / 1000).toFixed(2).replace(/\.0$/, '') + 'K';
        }
        return available_supply;
    };

    renderContent = (item) => {
        console.log("item=>" + JSON.stringify(item));
        return (
            <View style={{ width: 350, alignItems: 'flex-start', marginLeft: 5, }}>
                <TouchableOpacity
                    style={{ flex: 1, }}
                    onPress={() => console.log("hiii")}
                >
                    <View style={{ justifyContent: "center", alignItems: 'center' }}>
                        <ImageBackground
                            style={{
                                height: 150,
                                width: 330,
                                position: 'relative',
                            }}
                            source={{ uri: item.photo_file }}
                        >
                            <Text
                                style={{
                                    width: "100%",
                                    padding: 5,
                                    fontSize: 15,
                                    color: '#ffffff',
                                    backgroundColor: 'rgba(0,0,0,0.5)',
                                    alignItems: 'baseline',
                                    position: 'absolute',
                                    bottom: 0
                                }}
                            >
                                {item.title_en}
                            </Text>
                        </ImageBackground>
                    </View>
                </TouchableOpacity>
            </View>
        )
    };

    renderImpLinks = (ImpLinks) => {
        console.log("IMPLINKs" + ImpLinks)
        return (
            <View style={{ justifyContent: "center", alignItems: 'center' }}>
                <TouchableOpacity
                    style={styles.imagestyle}
                    onPress={() => Linking.openURL(ImpLinks.Link)}
                >
                    <Image style={styles.imagestyle} source={{ uri: ImpLinks.Image }} />
                </TouchableOpacity>
                <Text style={{ fontSize: 11 }}> {ImpLinks.name} </Text>
            </View>
        )
    };

    render() {
        const { CoinDetails, } = this.state;
        const { navigation, goBack, } = this.props;
        const key = navigation.getParam('CoinArray', 'NO-Symbol');
        // console.log("CoinDetail => " + JSON.stringify(CoinDetails))
        // console.log("keyvalue" + JSON.stringify(key));

        const ImpLinks = [
            {
                name: 'Website',
                ImpLinksuri: require('../Images/browser.png'),
                Link: this.state.CoinDetails.website,
            },
            {
                name: 'Explorer',
                ImpLinksuri: require('../Images/safari.png'),
                Link: this.state.CoinDetails.explorer,
            },
            {
                name: 'Facebook',
                ImpLinksuri: require('../Images/facebook.png'),
                Link: this.state.CoinDetails.facebook,
            },
            {
                name: 'Blog',
                ImpLinksuri: require('../Images/blogger.png'),
                Link: this.state.CoinDetails.blog,
            },
            {
                name: 'WhitePaper',
                ImpLinksuri: require('../Images/origami-white-page.png'),
                Link: this.state.CoinDetails.paper,
            },
            {
                name: 'Forum',
                ImpLinksuri: require('../Images/chat.png'),
                Link: this.state.CoinDetails.forum,
            },
            {
                name: 'Github',
                ImpLinksuri: require('../Images/github-logo.png'),
                Link: this.state.CoinDetails.github,
            },
            {
                name: 'Reddit',
                ImpLinksuri: require('../Images/reddit.png'),
                Link: this.state.CoinDetails.raddit,
            },
            {
                name: 'Slack',
                ImpLinksuri: require('../Images/slack.png'),
                Link: this.state.CoinDetails.slack,
            },
        ];
        // console.log("data =>" + JSON.stringify(ImpLinks));

        return (
            <View style={{ flex: 1 }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ flex: 1, flexDirection: 'column', }}>

                        {/* Header Container */}
                        <LinearGradient
                            colors={[key.color1, key.color2]}
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 0 }}
                        >
                            <View style={{ height: 150, alignItems: 'flex-start', flexDirection: 'column', padding: 10 }}>

                                {/* Lable Component */}
                                <View style={{ flex: 1, alignItems: 'flex-start', flexDirection: 'column' }}>

                                    {/* logo name symbol close */}
                                    <View style={{ height: 25, flexDirection: 'column', alignItems: 'stretch' }}>
                                        <View style={{ flex: 1, flexDirection: 'row', }}>
                                            <View style={{ width: "10%", alignItems: "center", justifyContent: 'center', width: 24, height: 24, borderRadius: 37.5, backgroundColor: '#f2f2f2', }}>
                                                <Image
                                                    source={{ uri: key.logo }}
                                                    style={{ width: 22, height: 22, borderRadius: 37.5, backgroundColor: '#f2f2f2', }}
                                                />
                                            </View>
                                            <View style={{ width: "80%", alignItems: "flex-start" }}>
                                                <Text style={{ fontSize: 15, fontStyle: 'normal', color: '#f2f2f2' }}>
                                                    {key.name}/{key.symbol}
                                                </Text>
                                            </View>
                                            <View style={{ width: "10%", alignItems: 'flex-end' }}>
                                                < TouchableOpacity
                                                    onPress={() => this._goback()}
                                                >
                                                    <SIcon
                                                        name='close'
                                                        size={25}
                                                        color='rgba(255, 255, 255, 0.8)'
                                                        backgroundColor='#f2f2f2'
                                                    />
                                                </ TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>

                                    {/* price percent_change24h */}
                                    <View style={{ height: 40, flexDirection: 'row', justifyContent: "center" }}>
                                        <View style={{ justifyContent: "center" }}>
                                            <Text style={{ fontSize: SCREEN_WIDTH * 0.09, fontWeight: 'bold', fontStyle: 'normal', color: '#ffffff' }}>
                                                ${key.price.toFixed(2)}
                                            </Text>
                                        </View>
                                        <View style={{ alignSelf: 'baseline', alignItems: "baseline" }}>
                                            <Text style={{ fontSize: SCREEN_WIDTH * 0.04, fontStyle: 'normal', color: '#ffffff', alignItems: "center" }}>
                                                {key.percent_change24h.toFixed(2)} %
                                                </Text>
                                        </View>
                                    </View>

                                    {/* BTC price value */}
                                    <View style={{ height: 10, justifyContent: "center" }}>
                                        <Text style={{ fontSize: SCREEN_WIDTH * 0.034, fontStyle: 'normal', color: '#ffffff', alignItems: "center" }}>
                                            B{key.price_btc.toFixed(6)}
                                        </Text>
                                    </View>

                                </View>

                                {/* 24 hrs Rates UP and DOWN 
                                Alert WhatchList and Card button */}
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 20 }}>
                                    <View style={{ height: 45, width: "50%", flexDirection: 'column', }}>
                                        <View style={{ flex: 1, flexDirection: 'column', }}>
                                            <View style={{ flex: 1, flexDirection: "row", alignItems: "flex-start" }}>
                                                <Text style={{ fontSize: SCREEN_WIDTH * 0.05, fontWeight: 'bold', color: '#f2f2f2' }}>
                                                    ${key.high24_usd.toFixed(2)}
                                                </Text>
                                                <Text style={{ fontSize: 12, color: '#f2f2f2' }}>
                                                    24 HRS HIGH
                                                </Text>
                                            </View>
                                            <View style={{ flex: 1, flexDirection: "row", alignItems: "flex-start" }}>
                                                <Text style={{ fontSize: SCREEN_WIDTH * 0.05, fontWeight: 'bold', color: '#f2f2f2' }}>
                                                    ${key.low24_usd.toFixed(2)}
                                                </Text>
                                                <Text style={{ fontSize: 11, color: '#f2f2f2', }}>
                                                    24 HRS HIGH
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ height: 45, width: "50%", flexDirection: 'column', }}>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly' }}>


                                            {/* WatchList Component */}
                                            <View style={{}}>
                                                < TouchableOpacity
                                                    style={styles.buttonStyle}
                                                    onPress={() => this.props.navigation.navigate("Alert")}
                                                >
                                                    <MIcon
                                                        style={{ alignItems: 'center', justifyContent: 'center' }}
                                                        name='alarm'
                                                        size={23}
                                                        color='#ffffff'
                                                        backgroundColor='#f2f2f2'
                                                    />
                                                </TouchableOpacity>
                                            </View>

                                            {/* Alert Component */}
                                            <View style={{}}>
                                                < TouchableOpacity
                                                    style={styles.buttonStyle}
                                                    onPress={() => this.props.navigation.navigate("Watchlist")}
                                                >
                                                    <Icon
                                                        style={{ alignItems: 'center', justifyContent: 'center' }}
                                                        name='star-outlined'
                                                        size={23}
                                                        color='#ffffff'
                                                        backgroundColor='#f2f2f2'
                                                    />
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{}}>
                                                < TouchableOpacity
                                                    style={styles.buttonStyle}
                                                    onPress={() => this.props.navigation.navigate("Alert")}
                                                >
                                                    <SIcon
                                                        style={{ alignItems: 'center', justifyContent: 'center' }}
                                                        name='frame'
                                                        size={23}
                                                        color='#ffffff'
                                                        backgroundColor='#f2f2f2'
                                                    />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </LinearGradient>
                    </View>
                    <ScrollView
                        style={{ height: SCREEN_WIDTH * 0.7 }}
                        contentContainerStyle={styles.scrollView}
                        onContentSizeChange={this.onContentSizeChange}
                    >
                        <View style={{ padding: 10, }}>
                            <View style={{ height: SCREEN_WIDTH * 0.33 }}>
                                <View style={styles.coinCapBoxStyle} >
                                    <View style={{ flexDirection: "row", justifyContent: "center", borderBottomWidth: 1, borderBottomColor: '#000000', borderRadius: 10 }}>
                                        <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", margin: 10 }}>
                                            <Text style={{ color: "#000000" }}>
                                                24 Hrs Volume
                                            </Text>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", margin: 10 }}>
                                            <Text style={{ color: "#0033cc" }}>
                                                $ {this.nFormatVolume(key.volume24h_usd === 0 ? CoinDetails.volume24h_usd : key.volume24h_usd)}
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: "row", justifyContent: "center", borderBottomWidth: 1, borderBottomColor: '#000000', borderRadius: 10 }}>
                                        <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", margin: 10 }}>
                                            <Text style={{ color: "#000000" }}>
                                                Total Coins
                                            </Text>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", margin: 10 }}>
                                            <Text style={{ color: "#0033cc" }}>
                                                {this.nFormatAvail(key.available_supply === "" ? CoinDetails.available_supply : key.available_supply)}
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: "row", justifyContent: "center" }}>
                                        <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-start", margin: 10 }}>
                                            <Text style={{ color: "#000000" }}>
                                                Market Cap
                                            </Text>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: "row", justifyContent: "flex-end", margin: 10 }}>
                                            <Text style={{ color: "#0033cc" }}>
                                                $ {this.nFormatMarket(key.market_cap_usd === "" ? CoinDetails.market_cap_usd : key.market_cap_usd)}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            </View>

                            {/* youtube and intro */}
                            <View style={{ marginTop: 10 }}>
                                <View style={styles.coinCapBoxStyle} >
                                    <View style={{ flex: 1, }}>
                                        <Text style={{ padding: 10, color: '#000000' }}>
                                            About
                                        </Text>
                                        <Text numberOfLines={4} ellipsizeMode={'tail'} style={{ padding: 10, }}>
                                            {CoinDetails.intro ? CoinDetails.intro.replace(/(<([^>]+)>)/ig, '\n') : ""}
                                        </Text>
                                    </View>
                                    <View style={{ height: 300 }}>
                                        <WebView
                                            source={{ uri: 'https://www.youtube.com/embed/' + CoinDetails.youtube }}
                                            style={{ marginTop: 20 }}
                                        />
                                    </View>
                                    {CoinDetails.youtube === "" ? console.log("item Get=>",CoinDetails.youtube) : console.log("item Get=>",CoinDetails.youtube)}
                                </View>
                            </View>

                            {/* important Links */}
                            <View style={{ marginTop: 10 }}>
                                <View style={styles.coinCapBoxStyle} >
                                    <View>
                                        <Text style={{ padding: 10, color: '#000000' }}>
                                            Important Links
                                        </Text>
                                    </View>
                                    <View style={{ justifyContent: 'center', flex: 1, padding: 10 }}>
                                        <FlatList
                                            data={ImpLinks}
                                            keyExtractor={(item, index) => index}
                                            renderItem={({ item }) => (
                                                <View style={{ flex: 1, flexDirection: 'column', margin: 10, }}>
                                                    <TouchableOpacity
                                                        style={styles.imagestyle}
                                                        onPress={() => Linking.openURL(item.Link)}
                                                    >
                                                        <Image style={styles.imagestyle} source={item.ImpLinksuri} />
                                                    </TouchableOpacity>
                                                    <Text style={{ fontSize: 11, }}> {item.name} </Text>
                                                </View>
                                            )}
                                            numColumns={4}
                                        />
                                    </View>
                                </View>
                            </View>

                            {/* Important Articles */}
                            <View style={{ marginTop: 10 }}>
                                <View style={styles.coinCapBoxStyle} >
                                    <View>
                                        <Text style={{ padding: 10, color: '#000000' }}>
                                            Important Articles
                                        </Text>
                                    </View>
                                    <View>
                                        <FlatList
                                            style={{ display: "flex", flexDirection: 'row', }}
                                            horizontal={true}
                                            data={CoinDetails.guides}
                                            showsHorizontalScrollIndicator={true}
                                            keyExtractor={(item, index) => index}
                                            renderItem={({ item }) => (this.renderContent(item))}
                                        >

                                        </FlatList>
                                    </View>
                                </View>
                            </View>

                        </View>
                    </ScrollView>
                </SafeAreaView>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: 'white',
        flexGrow: 1,
    },
    loader: {
        marginTop: 10,
        alignItems: "center"
    },
    buttonStyle: {
        borderRadius: 12,
        shadowOpacity: 10,
        padding: 10,
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
    },
    coinCapBoxStyle: {
        flex: 1,
        borderRadius: 10,
        elevation: 3,
        flexDirection: "column",
        overflow: 'hidden'
    },
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: '#ffffff'
    },
    imagestyle: {
        borderRadius: 20,
        height: 50,
        width: 50,
    },

})