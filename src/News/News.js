import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    StyleSheet,
    SafeAreaView,
    Image,
    TouchableOpacity,
    Dimensions,
    ActivityIndicator,
    LayoutAnimation,
    UIManager,
    Platform,
    ImageBackground,
    Share,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import PageControl from 'react-native-page-control'
import Icon from 'react-native-vector-icons/AntDesign';

const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
} = Dimensions.get('window');

var CustomLayoutAnimation = {
    duration: 500,
    create: {
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.opacity,
    },
    update: {
        type: LayoutAnimation.Types.easeInEaseOut,
        property: LayoutAnimation.Properties.opacity,
    },
    delete: {
        duration: 500,
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.opacity,
    },
};
var previousScrollOffset = 0;

export default class News extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: true,
            NewsList: [],
            refreshing: false,
            FeaturedNewsList: [],
            visible: false,
            offset: 0,
            featuredNewHeight: 140,
            visibleFeatureNewsCount: 0,
            currentId: null,
        }
        this.onPressExapand = this.onPressExapand.bind(this);
        if (
            Platform.OS === 'android' &&
            UIManager.setLayoutAnimationEnabledExperimental
        ) {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        };
    };


    componentDidMount() {
        this.getListCoin();
    };

    // detail Newslist of coin
    getListCoin = () => {
        const url = 'https://api.cointopper.com/api/v3/news?limit=10&offset=' + this.state.offset;
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                responseJson = responseJson.data.map(item => {
                    item.isSelect = false;
                    
                    return item;
                  });
               
                this.setState({
                    NewsList: this.state.NewsList.concat(responseJson)
                })
            })
            .catch((error) => {
                console.log(error)
            })
    }

    // for Featured News
    componentWillMount() {
        const url = 'https://api.cointopper.com/api/v3/featured'
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    FeaturedNewsList: responseJson.data
                })
            })
            .catch((error) => {
                console.log(error)
            })
    };

    // Featured News Button
    renderFeaturedNewsContent = (item) => {
        // console.log("item=>>>" + JSON.stringify(item));
        return (
            <View style={{ height: this.state.featuredNewHeight, alignItems: 'flex-start', marginLeft: 5, }}>
                <TouchableOpacity
                    style={{ flex: 1, }}
                    onPress={() => this.props.navigation.navigate('NewsDetailScreen', {
                        NewsArray: item,
                    })}
                >
                    <View style={{ justifyContent: "center", alignItems: 'center' }}>
                        <ImageBackground
                            style={{
                                height: 130,
                                width: SCREEN_WIDTH - 20,
                                position: 'relative',
                            }}
                            source={{ uri: item.photo_file }}
                        >
                            <Text
                                style={{
                                    width: "100%",
                                    padding: 5,
                                    fontSize: 15,
                                    color: '#ffffff',
                                    backgroundColor: 'rgba(0,0,0,0.5)',
                                    alignItems: 'baseline',
                                    position: 'absolute',
                                    bottom: 0
                                }}
                            >
                                {item.title_en}
                            </Text>
                        </ImageBackground>
                    </View>
                </TouchableOpacity>
            </View>
        )
    };

    onPressExapand=(item)=> {
        console.log("Item =>"+JSON.stringify(item));
        item.isSelect=!item.isSelect;
        const index = this.state.NewsList.findIndex(
            (singleitem) => {
                item.id === singleitem.id
                console.log("item.id=>"+singleitem.id)
            }
          );
          console.log("item=>>"+index);
          
          this.state.NewsList[index] = item;
        
          this.setState({
            NewsList: this.state.NewsList,
          });
          
    }
    // NewsList 
    renderNewsList = (item) => {
        // console.log("item ==>> " + item.name);
        // console.log("item"+JSON.stringify(item));
        return (
            <View style={{ height: 'auto', width: 'auto', }}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('NewsDetailScreen', {
                        NewsArray: item,
                    })}
                >
                    <View
                        style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexDirection: 'row',
                            borderBottomWidth: 0.5,
                            padding: 5,
                            borderBottomColor: 'grey',
                        }}>
                        <View style={{ width: "20%", alignItems: 'flex-start', justifyContent: 'flex-start', flexDirection: 'row', }}>
                            <View style={{ flex: 1, justifyContent:'flex-start' }}>
                                <Image
                                    source={{ uri: item.photo_file }}
                                    style={{ width: 70, height: 70, }}
                                />
                            </View>
                        </View>
                        <View style={{ width: "80%", alignItems: 'flex-start', justifyContent: 'flex-start', flexDirection: 'row', padding: 10, }}>
                            <View style={{ flex: 1, flexDirection: "column", }}>
                                <View style={{ height: 'auto', paddingLeft: 10 }}>
                                    <Text > {item.title_en} </Text>
                                    {item.isSelect === true ? <Text style={{ fontSize: 12 }}> {item.seo_description_en} </Text> : null}
                                </View>
                                <View style={{ height: 40, flexDirection: "row", padding: 10, }}>
                                    <View style={{ width: "50%" }}>
                                        <Text style={{ fontSize: 13 }}> {item.date} </Text>
                                    </View>
                                    <View style={{ width: "25%" }}>
                                        <TouchableOpacity
                                            style={{ width: 50, }}
                                            onPress={() => this.ShareMessage(item)}
                                        >
                                            <Text>
                                                Share
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ width: "25%" }}>
                                        <TouchableOpacity
                                            style={{ width: 50,alignSelf:'flex-end' }}
                                            onPress={() => this.onPressExapand(item)}
                                        >
                                            {item.isSelect === true ?
                                                <Icon
                                                    name='up'
                                                    size={20}
                                                    style={{ alignSelf:'flex-end' }}
                                                />
                                                :
                                                <Icon
                                                    name='down'
                                                    size={20}
                                                    style={{ alignSelf:'flex-end' }}
                                                />
                                            }
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    };

    renderNewsListFooter = () => {

        return (
            <View style={styles.loader}>
                <ActivityIndicator size="large"></ActivityIndicator>
            </View>
        )
    }

    handleLoadMore = () => {
        this.setState(
            {
                offset: this.state.offset + 5
            },
            this.getListCoin
        )
    }

    onScroll = (event) => {
        var currentOffset = event.nativeEvent.contentOffset.y;
        if (currentOffset < 0) {
            return;
        }
        var direction = currentOffset > this.previousScrollOffset ? 'up' : 'down';
        this.previousScrollOffset = currentOffset;
        // console.log("this.previousScrollOffset:" + this.previousScrollOffset)
        // console.log(this.offset);
        if (direction === 'down') {
            // console.log('down')
            // LayoutAnimation.configureNext(CustomLayoutAnimation);
            if (this.previousScrollOffset === 0) {
                this.setState({
                    featuredNewHeight: 140,
                })
            }
        } else {
            // console.log('up')
            LayoutAnimation.configureNext(CustomLayoutAnimation);
            this.setState({
                featuredNewHeight: 0,
            })
        }

    };

    ShareMessage = (item) => {
        // console.log("ShareMessage=>>" + JSON.stringify(item));
        Share.share({
            title: `${item.title_en}`,
            message: `${item.post_link}`,
        })
            .then(result => console.log(result))
            .catch(errorMsg => console.log(errorMsg));
    };

    onScrollEnd(e) {
        let contentOffset = e.nativeEvent.contentOffset;
        let viewSize = e.nativeEvent.layoutMeasurement;
        // console.log("contentOffset.x:-"+contentOffset.x)
        // console.log("viewSize.width"+viewSize.width)
        // console.log("contentOffset.x / viewSize.width:-"+contentOffset.x / viewSize.width)
        let pageNum = Math.round(contentOffset.x / viewSize.width);
        // console.log('scrolled to page ', pageNum);
        this.setState({
            visibleFeatureNewsCount: pageNum,
        });
    }

    render() {
        const { NewsList, FeaturedNewsList } = this.state;
        const { navigate } = this.props.navigation;
        // console.log("height animation =>>" + this.state.featuredNewHeight)
        // console.log("FeaturedNewsList:-" + JSON.stringify(FeaturedNewsList));
        // console.log("News List:-" + JSON.stringify(FeaturedNewsList));
        return (
            <View style={{ flex: 1 }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ flex: 1, flexDirection: 'column', }}>

                        {/* Header Container */}
                        <LinearGradient
                            colors={['#ff00ff', '#ff0066']}
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 0 }}
                        >
                            <View style={{
                                height: 150,
                                alignItems: 'flex-start',
                                flexDirection: 'column',
                                padding: 10
                            }}>

                                {/* Lable Component */}
                                <View style={{ flex: 1, alignItems: 'flex-start', flexDirection: 'row' }}>
                                    <View style={{ flexDirection: 'column', alignItems: 'stretch' }}>
                                        <Text style={{ fontSize: 17, fontStyle: 'normal', color: '#f2f2f2' }}>
                                            CoinTopper
                                    </Text>
                                        <Text style={{
                                            fontSize: SCREEN_WIDTH * 0.09,
                                            fontWeight: 'bold',
                                            fontStyle: 'normal',
                                            color: '#ffffff'
                                        }}>
                                            News
                                    </Text>
                                    </View>
                                </View>

                                {/* Search, */}
                                <View style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>

                                    {/* Search Component */}
                                    <View style={{ flex: 1 }}>
                                        <TouchableOpacity
                                            style={styles.buttonStyle}
                                            onPress={() => this.props.navigation.navigate('Search')}
                                        >
                                            <Text style={{
                                                color: '#e6e6e6',
                                                textAlign: 'left',
                                                justifyContent: 'flex-start'
                                            }}>
                                                Search
                                        </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </LinearGradient>

                        {/* Featured News List Container  */}
                        <View style={{
                            display: this.state.featuredNewHeight === 0 ? 'none' : 'flex',
                            alignItems: 'flex-start',
                            flexDirection: 'column',
                            borderBottomWidth: 1,
                            padding: 5,
                            borderColor: "#d9d9d9"
                        }}>
                            <View style={{ marginBottom: 5 }}>
                                <Text style={{ fontSize: 13, color: '#005580' }}>
                                    FEATURED NEWS
                                </Text>
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: 'flex-start', }}>
                                <FlatList
                                    style={{ display: 'flex', flexDirection: 'row', }}
                                    pagingEnabled={true}
                                    horizontal={true}
                                    data={this.state.FeaturedNewsList}
                                    showsHorizontalScrollIndicator={false}
                                    keyExtractor={(item, index) => index.toString()}
                                    renderItem={({ item }) => (this.renderFeaturedNewsContent(item))}
                                    // onViewableItemsChanged={this.onViewableItemsChanged}
                                    // viewabilityConfig={{
                                    //     itemVisiblePercentThreshold: 50
                                    // }}
                                    onMomentumScrollEnd={(e) => this.onScrollEnd(e)}
                                />
                                <PageControl
                                    style={{ position: 'absolute', left: 0, right: 0, bottom: 0 }}
                                    numberOfPages={5}
                                    pagingEnabled
                                    currentPage={this.state.visibleFeatureNewsCount}
                                    hidesForSinglePage
                                    pageIndicatorTintColor='gray'
                                    currentPageIndicatorTintColor='red'
                                    indicatorStyle={{ borderRadius: 5 }}
                                    currentIndicatorStyle={{ borderRadius: 5 }}
                                    indicatorSize={{ width: 8, height: 8 }}
                                    onPageIndicatorPress={this.onItemTap}
                                />

                            </View>
                        </View>

                        {/* News List Cointainer */}
                        <View style={{ flex: 1, flexDirection: 'column', }}>
                            <View style={{ height: 40, flexDirection: 'row', padding: 10 }}>
                                <Text>RECENT NEWS</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: "column" }}>
                                <FlatList
                                    windowSize={50}
                                    removeClippedSubviews={true}
                                    maxToRenderPerBatch={10}
                                    updateCellsBatchingPeriod={5}
                                    onScroll={this.onScroll}
                                    style={{ flex: 1 }}
                                    data={NewsList}
                                    keyExtractor={(item) => item.id}
                                    renderItem={({ item }) => (this.renderNewsList(item))}
                                    onEndReached={this.handleLoadMore}
                                    onEndReachedThreshold={0}
                                    ListFooterComponent={this.renderNewsListFooter}
                                />
                            </View>
                        </View>

                    </View>
                </SafeAreaView>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    loader: {
        marginTop: 10,
        alignItems: "center"
    },
    buttonStyle: {
        borderRadius: 12,
        shadowOpacity: 10,
        padding: 10,
        backgroundColor: '#ff4d94',
    },
})