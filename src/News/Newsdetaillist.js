import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    Image,
    ImageBackground,
    TouchableOpacity,
    Dimensions,
    Share,
} from "react-native";
import AutoHeightWebView from 'react-native-autoheight-webview'
import SIcon from 'react-native-vector-icons/SimpleLineIcons'
import LinearGradient from 'react-native-linear-gradient';
const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
} = Dimensions.get('window');

export default class Newsdetaillist extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: true,
            NewsDetails: [],
            screenHeight: SCREEN_HEIGHT,
            ApiLink: "",
            WebviewHeight: 0,
        }
    };

    componentDidMount() {
        this.getListCoin();
    };

    getListCoin = () => {
        const { navigation } = this.props;
        const ApiLink = 'https://api.cointopper.com/api/v3/news/';
        const key = navigation.getParam('NewsArray', 'NO-Symbol');
        const url = ApiLink + key.id;
        // console.log("URL=> " + url);
        // console.log("key" + key);
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    NewsDetails: responseJson.data,
                })
            })
            .catch((error) => {
                console.log(error)
            })
    };

    _goback() {
        const { goBack } = this.props.navigation;
        goBack();
    }

    onContentSizeChange = (contentWidth, contentHeight) => {
        this.setState({ screenHeight: contentHeight });
    };

    renderContent = (item) => {
        console.log("item=>" + JSON.stringify(item));
        return (
            <View style={{ width: 350, alignItems: 'flex-start', marginLeft: 5, }}>
                <TouchableOpacity
                    style={{ flex: 1, }}
                    onPress={() => console.log("hiii")}
                >
                    <View style={{ justifyContent: "center", alignItems: 'center' }}>
                        <ImageBackground
                            style={{
                                height: 150,
                                width: 330,
                                position: 'relative',
                            }}
                            source={{ uri: item.photo_file }}
                        >
                            <Text
                                style={{
                                    width: "100%",
                                    padding: 5,
                                    fontSize: 15,
                                    color: '#ffffff',
                                    backgroundColor: 'rgba(0,0,0,0.5)',
                                    alignItems: 'baseline',
                                    position: 'absolute',
                                    bottom: 0
                                }}
                            >
                                {item.title_en}
                            </Text>
                        </ImageBackground>
                    </View>
                </TouchableOpacity>
            </View>
        )
    };

    ShareMessage = (NewsDetails) => {
        console.log("ShareMessage=>>" + JSON.stringify(NewsDetails));
        Share.share({
            title: `${NewsDetails.title_en}`,
            message: `${NewsDetails.post_link}`,
        })
            .then(result => console.log(result))
            .catch(errorMsg => console.log(errorMsg));
    };

    render() {
        const { NewsDetails, } = this.state;
        const { navigation, goBack, } = this.props;
        const key = navigation.getParam('NewsArray', 'NO-Symbol');
        // console.log("NewsDetail => " + JSON.stringify(NewsDetails))
        // console.log("keyvalue" + JSON.stringify(key));

        return (

            <View style={{ flex: 1 }}>
                <SafeAreaView style={{ flex: 1 }}>
                    {/* Header Container */}
                    <LinearGradient
                        colors={['#ff00ff', '#ff0066']}
                        start={{ x: 0, y: 1 }}
                        end={{ x: 1, y: 0 }}
                    >
                        <View style={{ height: 45, padding: 10, alignSelf: "center" }}>
                            {/* logo name symbol close */}
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'stretch' }}>
                                <View style={{ height: 30, flexDirection: 'row', }}>
                                    <View style={{ width: "90%", alignItems: "flex-start" }}>
                                        <Text style={{ fontSize: 15, fontStyle: 'normal', color: '#f2f2f2' }}>
                                            News
                                                </Text>
                                    </View>
                                    <View style={{ width: "10%", alignItems: "flex-end" }}>
                                        < TouchableOpacity
                                            onPress={() => this._goback()}
                                        >
                                            <SIcon
                                                name='close'
                                                size={25}
                                                color='rgba(255, 255, 255, 0.8)'
                                                backgroundColor='#f2f2f2'
                                            />
                                        </ TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </LinearGradient>


                    <ScrollView
                        style={{ height: SCREEN_WIDTH * 0.7 }}
                        contentContainerStyle={styles.scrollView}
                        onContentSizeChange={this.onContentSizeChange}
                    >
                        {/* NewsTitle */}
                        <LinearGradient
                            colors={['#ff00ff', '#ff0066']}
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 0 }}
                        >
                            <View style={{
                                height: 'auto', width: 'auto', justifyContent: 'center',
                                alignItems: 'center', padding: 10,
                            }}>
                                <Text style={{
                                    fontSize: SCREEN_WIDTH * 0.09,
                                    alignSelf: 'center',
                                    padding: 10,
                                    fontWeight: 'bold',
                                    fontStyle: 'normal',
                                    color: '#f2f2f2'
                                }}>
                                    {NewsDetails.title_en}
                                </Text>
                            </View>
                        </LinearGradient>

                        {/* Image, Share, date, WebView NewsDetail */}
                        <View style={{
                            flex: 1
                        }}>
                            {/* Image, Share, Date */}
                            <View style={{ height: 250, alignItems: 'center' }}>
                                <Image
                                    source={{ uri: (key.photo_file === 0 ? NewsDetails.photo_file : key.photo_file) }}
                                    style={{ width: "100%", height: 220 }}
                                />
                                <View style={{ height: 30, flexDirection: "row", }}>
                                    <View style={{ width: "50%", alignSelf: 'flex-start' }}>
                                        <TouchableOpacity
                                            style={{ height: 30, alignSelf: 'flex-start', padding: 10 }}
                                            onPress={() => this.ShareMessage(NewsDetails)}
                                        >
                                            <Text>
                                                Share
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ width: "50%", alignSelf: 'flex-end' }}>
                                        <Text style={{ fontSize: 13, alignSelf: 'flex-end' }}> {key.date === 0 ? NewsDetails.date : key.date} </Text>
                                    </View>
                                </View>
                            </View>

                            {/* NewsDetail */}
                            <View style={{ flex: 1 }}>
                                <AutoHeightWebView
                                    style={{ width: Dimensions.get('window').width - 15, }}
                                    customScript={`document.body.style.background = 'white';`}
                                    customStyle={`
                                        * {
                                            font-family: 'Times New Roman';
                                        }
                                        p {
                                            font-size: 16px;
                                        }
                                    `}
                                    onSizeUpdated={size => console.log(size.height)}
                                    files={[{
                                        href: 'cssfileaddress',
                                        type: 'text/css',
                                        rel: 'stylesheet'
                                    }]}
                                    source={{ html: NewsDetails.details_en }}
                                    zoomable={false}
                                />
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: 'white',
        flexGrow: 1,
    },
    loader: {
        marginTop: 10,
        alignItems: "center"
    },
    buttonStyle: {
        borderRadius: 12,
        shadowOpacity: 10,
        padding: 10,
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
    },
    coinCapBoxStyle: {
        flex: 1,
        borderRadius: 10,
        elevation: 3,
        flexDirection: "column",
        overflow: 'hidden'
    },
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: '#ffffff'
    },
    imagestyle: {
        borderRadius: 20,
        height: 50,
        width: 50,
    },

})