import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator, } from 'react-navigation';
import News from './News';
import search from '../Components/Search';
import newsdetaillist from './Newsdetaillist';
const NewsHome = createStackNavigator(
    {
        news: {
            screen: News,
            navigationOptions: {
                header: null,
            },
        },
        Search: {
            screen: search,
            navigationOptions: {
                header: null,
            },
        },
        NewsDetailScreen: {
            screen: newsdetaillist,
            navigationOptions: {
                header: null,
            },
        },
    },
    {
        // navigationOptions: ({ navigation }) => ({
        //     header: null,
        //     tabBarVisible: false,
        // }),
    },
);


NewsHome.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        return {
        tabBarVisible: false
      };
    }
  
    return {
      tabBarVisible,
    };
  };

export default NewsHome;