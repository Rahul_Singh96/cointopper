import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator, } from 'react-navigation';
import Home from './Home';
import search from '../Components/Search';
import WatchList from '../Components/WatchList';
import Alerts from '../Components/Alerts';
import TopViewed from '../Components/TopViewedCoin';
import CoinDetailpage from '../Components/CoinDetailpage';

const Home_1 = createStackNavigator(
    {
        Home: {
            screen: Home,
            navigationOptions: {
                header: null,
            },
        },
        Search: {
            screen: search,
            navigationOptions: {
                header: null,
            },
        },
        Watchlist: {
            screen: WatchList,
            // navigationOptions: {
            //     header: null,
            // },
        },
        Alert: {
            screen: Alerts,
            navigationOptions: {
                header: null,
            },
        },
        topviewed: {
            screen: TopViewed,
            navigationOptions: {
                header: null,
            },
        },
        CoinDetailPage: {
            screen: CoinDetailpage,
            navigationOptions: {
                header: null,
            },
        }

    },
    {
        // navigationOptions: ({ navigation }) => ({
        //     header: null,
        //     tabBarVisible: false,
        // }),
    },
);


Home_1.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        return {
        tabBarVisible: false
      };
    }
  
    return {
      tabBarVisible,
    };
  };

export default Home_1;