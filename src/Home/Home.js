import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    StyleSheet,
    SafeAreaView,
    Image,
    TouchableOpacity,
    Dimensions,
    ActivityIndicator,
    LayoutAnimation,
    UIManager,
    Platform,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import LinearGradient from 'react-native-linear-gradient';
const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
} = Dimensions.get('window')
var CustomLayoutAnimation = {
    duration: 500,
    create: {
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.opacity,
    },
    update: {
        type: LayoutAnimation.Types.linear,
    },
    delete: {
        duration: 500,
        type: LayoutAnimation.Types.linear,
        property: LayoutAnimation.Properties.opacity,
    },
};
export default class Home extends Component {

    constructor() {
        super();
        this.state = {
            isLoading: true,
            dataSource: [],
            refreshing: false,
            nameSort: true,
            dataList: [],
            topsearch: [],
            picked: 'USD',
            visible: false,
            offset: 0,
            topSearchCoinViewHeight: 120,
        }
        if (Platform.OS === 'android') {

            UIManager.setLayoutAnimationEnabledExperimental(true);

        }
        this.sortList.bind(this);
        this.compareBy.bind(this);
    };

    componentDidMount() {
        this.getListCoin();
    };
    // detail list of coin
    getListCoin = () => {
        const url = 'https://api.cointopper.com/api/v3/ticker?offset=' + this.state.offset;
        // console.log("url",url);
        // console.log("offset",this.state.offset)
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    dataSource: this.state.dataSource.concat(responseJson.data)
                })
            })
            .catch((error) => {
                console.log(error)
            })
    }
    // for top viewed
    componentWillMount() {
        const url = 'https://api.cointopper.com/api/v3/topsearched'
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {

                let topsearch = responseJson.data;
                // console.log("test1 =>>" + topsearch.length);
                if (topsearch.length >= 5) {
                    topsearch.length = 5;
                    // console.log("5 Search array Len:-" + topsearch.length)
                    // console.log("5 Search array:-" + JSON.stringify(topsearch))
                    this.setState({
                        dataList: topsearch
                    });
                } else {
                    this.setState({
                        dataList: topsearch
                    });
                }
            })
            .catch((error) => {
                console.log(error)
            })
    };

    nFormatter(market_cap_usd) {
        if (market_cap_usd >= 1000000000) {
            return (market_cap_usd / 1000000000).toFixed(2) + 'B';
        }
        if (market_cap_usd >= 1000000) {
            return (market_cap_usd / 1000000).toFixed(2) + 'M';
        }
        if (market_cap_usd >= 1000) {
            return (market_cap_usd / 1000).toFixed(2).replace(/\.0$/, '') + 'K';
        }
        return market_cap_usd;
    };

    compareBy = (key) => {
        return function (a, b) {
            if (a[key] < b[key]) return -1;
            if (a[key] > b[key]) return 1;
            return 0;
        };
    };

    sortList = (key) => {
        let arrayCopy = [...this.state.dataSource];
        // console.log(
        //     arrayCopy.map(item => {
        //         return item.name;
        //     })
        // );
        if (this.state.nameSort === true) {
            arrayCopy.sort(this.compareBy(key));
            this.setState({
                nameSort: false,
                dataSource: arrayCopy
            });
        } else {
            arrayCopy.sort(this.compareBy(key)).reverse();
            this.setState({
                nameSort: true,
                dataSource: arrayCopy
            });
        } return 0;

    };

    renderfooter = () => {
        return (
            <View style={{ width: 150, marginLeft: 5, }}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate("topviewed")}
                    title="Tab Two"
                    style={styles.ListButtonStyle}
                >
                    <Text
                        style={{
                            textAlign: 'center',
                            fontSize: 18,
                            color: '#3333cc',
                        }}
                    >
                        View all
                    </Text>
                </TouchableOpacity>
            </View>
        );
    };

    // topViewed Button
    renderContent = (item) => {
        return (
            <View style={{ width: 150, alignItems: 'flex-start', marginLeft: 5, marginRight: 5 }}>
                <TouchableOpacity
                    style={{ flex: 1, alignContent: 'space-around' }}
                    onPress={() => this.props.navigation.navigate('CoinDetailPage', {
                        CoinArray: item,
                    })}
                >
                    <LinearGradient
                        colors={[item.color1, item.color2]}
                        style={{ width: "100%", height: "100%", borderRadius: 15 }}
                        start={{ x: 1, y: 0 }}
                        end={{ x: 0, y: 1 }}
                    >
                        <View
                            style={{
                                width: "100%", height: "100%",
                                flexDirection: 'row',
                                paddingLeft: 10
                            }}>
                            <View style={{ flex: 3, flexDirection: 'column', justifyContent: 'center', alignContent: 'space-around', margin: 0, padding: 0 }}>
                                <Text style={{ color: '#ffffff', fontSize: 11, }}>{item.name}</Text>
                                <Text style={{ color: '#ffffff', fontSize: 18, fontWeight: 'bold' }}>${item.price.toFixed(2)}</Text>
                                <Text style={{ color: '#ffffff', fontSize: 11, }}>{item.percent_change24h.toFixed(2)} %</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'flex-end', alignSelf: 'flex-end', paddingBottom: 5, paddingRight: 5 }}>
                                <Image
                                    source={{ uri: item.logo }}
                                    style={{ width: 35, height: 35, borderRadius: 37.5, backgroundColor: '#f2f2f2', }}
                                />
                            </View>
                        </View>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        )
    };

    _onRefresh = () => {
        const url = 'https://api.cointopper.com/api/v3/ticker'
        this.setState({
            dataSource: [],
            refreshing: true
        })
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log("Refresh Data:-" + responseJson.data);
                this.setState({
                    dataSource: responseJson.data,
                    refreshing: false
                })
            })
            .catch((error) => {
                console.log(error)
            })
    };

    renderItemCoin = (item) => {
        // console.log("item ==>> " + item.name);
        return (
            <View>
                <TouchableOpacity
                    // style={{ flex: 1, alignContent: 'space-around' }}
                    onPress={() => this.props.navigation.navigate('CoinDetailPage', {
                        CoinArray: item,
                    })}
                >
                    <View
                        style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexDirection: 'row',
                            borderBottomWidth: 0.5,
                            padding: 5,
                            borderBottomColor: 'grey',
                        }}>
                        <View style={{ flex: 3, alignItems: 'flex-start', justifyContent: 'flex-start', flexDirection: 'row' }}>
                            <View style={{ flex: 1, alignItems: 'center' }}>
                                <Image
                                    source={{ uri: item.logo }}
                                    style={{ width: 30, height: 30, borderRadius: 37.5 }}
                                />
                            </View>

                            <View key={item.name} style={{ flex: 3, flexDirection: "column", alignItems: 'flex-start', justifyContent: 'flex-start', }}>
                                <Text style={{ color: '#001a33' }}>{item.name}</Text>
                                <View key={item.market_cap_usd} style={{ flex: 3, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start', }}>
                                    <Text style={{ color: '#a6a6a6' }}>{item.symbol} / {this.nFormatter(item.market_cap_usd)}</Text>
                                </View>
                            </View>
                        </View>
                        <View key={item.percent_change24h} style={{ flex: 2, alignItems: 'center', justifyContent: 'center', }}>
                            <Text style={{ color: '#001a33' }}>{item.percent_change24h.toFixed(2)} %</Text>
                        </View>
                        <View key={item.price} style={{ flex: 2, justifyContent: 'flex-start', alignItems: 'flex-start', flexDirection: 'column' }}>
                            <Text style={{ color: '#005580' }}>$ {item.price.toFixed(2)}</Text>
                            <Text style={{ color: '#a6a6a6' }}>B {item.price_btc.toFixed(8)}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    };

    renderItemCoinFooter = () => {

        return (
            <View style={styles.loader}>
                <ActivityIndicator size="large"></ActivityIndicator>
            </View>
        )
    }

    handleLoadMore = () => {
        // console.warn("handle more");
        this.setState(
            {
                offset: this.state.offset + 20
            },
            this.getListCoin
        )
    }

    onScroll = (event) => {
        var currentOffset = event.nativeEvent.contentOffset.y;
        var direction = currentOffset > this.offset ? 'up' : 'down';
        this.offset = currentOffset;
        // console.log(this.offset);
        if (direction === 'down') {
            // console.log('down')
            LayoutAnimation.configureNext(CustomLayoutAnimation);
            if (this.offset === 0) {
                this.setState({
                    topSearchCoinViewHeight: 120
                })
            }
        } else {
            // console.log('up')
            LayoutAnimation.configureNext(CustomLayoutAnimation);
            this.setState({
                topSearchCoinViewHeight: 0
            })
        }

    }

    render() {
        const { dataSource, dataList, topsearch } = this.state;
        const { navigate } = this.props.navigation;
        // console.log("Coin List:-" + JSON.stringify(dataSource));
        const CoinName = [
            {
                key: 'AUD',
                label: 'Australian Dollar (AUD)',
            },
            {
                key: 'BRL',
                label: 'Brazilian Real (BRL)',
            },
            {
                key: 'GBP',
                label: 'British Pound Sterling (GBP)',
            },
            {
                key: 'CAD',
                label: 'Canadian Dollar (CAD)',
            },
            {
                key: 'CNY',
                label: 'Chinese Yuan (CNY)',
            },
            {
                key: 'EUR',
                label: 'Euro (EUR)',
            },
            {
                key: 'HKD',
                label: 'Hong Kong Dollar (HKD)',
            },
            {
                key: 'INR',
                label: 'Indian Rupee (INR)',
            },
            {
                key: 'IDR',
                label: 'Indonesian Rupiah (IDR)',
            },
            {
                key: 'JPY',
                label: 'Japanese Yen (JPY)',
            },
            {
                key: 'PLN',
                label: 'Polish Zloty (PLN)',
            },
            {
                key: 'RUB',
                label: 'Russian Ruble (RUB)',
            },
            {
                key: 'ZAR',
                label: 'South African Rand (ZAR)',
            },
            {
                key: 'KRW',
                label: 'South Korean Won (KRW)',
            },
            {
                key: 'TRY',
                label: 'Turkish Lira (TRY)',
            },
            {
                key: 'USD',
                label: 'US Dollar (USD)',
            },
        ];

        return (
            <View style={{ flex: 1 }}>
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={{ flex: 1, flexDirection: 'column', }}>

                        {/* Header Container */}
                        <LinearGradient
                            colors={['#003399', '#0073e6']}
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 0 }}
                        >
                            <View style={{ height: 150, alignItems: 'flex-start', flexDirection: 'column', padding: 10 }}>

                                {/* Lable Component */}
                                <View style={{ flex: 1, alignItems: 'flex-start', flexDirection: 'row' }}>
                                    <View style={{ flexDirection: 'column', alignItems: 'stretch' }}>
                                        <Text style={{ fontSize: 17, fontStyle: 'normal', color: '#f2f2f2' }}>
                                            Welcome to CoinTopper
                                    </Text>
                                        <Text style={{ fontSize: SCREEN_WIDTH * 0.09, fontWeight: 'bold', fontStyle: 'normal', color: '#ffffff' }}>
                                            Cryptocurrencies
                                    </Text>
                                    </View>
                                    <View style={{ flexDirection: 'column', alignItems: 'flex-end' }}>
                                        <Text style={{ fontSize: 10, fontStyle: 'normal', color: '#f2f2f2' }}>
                                            CRYPTO M.CAP
                                    </Text>
                                        <Text style={{ fontSize: 13, fontStyle: 'normal', color: '#f2f2f2' }}>
                                            $1000.00 B
                                    </Text>
                                    </View>
                                </View>

                                {/* Search, List Picker, WatchList, Alert */}
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>

                                    {/* Search Component */}
                                    <View style={{ flex: 1 }}>
                                        <TouchableOpacity
                                            style={styles.buttonStyle}
                                            onPress={() => this.props.navigation.navigate('Search')}
                                        >
                                            <Text style={{ color: '#e6e6e6', textAlign: 'left', justifyContent: 'flex-start' }}>
                                                Search
                                        </Text>
                                        </TouchableOpacity>
                                    </View>

                                    {/* List Picker Component, WatchList, Alert Component */}
                                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around' }}>
                                        <View style={{ width: "40%" }}>
                                            <TouchableOpacity style={styles.PickerStyle} onPress={this.onShow}>
                                                <Text style={{ color: '#e6e6e6', padding: 10, fontSize: 14, }}>
                                                    {this.state.picked}
                                                </Text>
                                            </TouchableOpacity>
                                            <ModalFilterPicker
                                                showFilter={false}
                                                visible={this.state.visible}
                                                onSelect={this.onSelect}
                                                onCancel={this.onCancel}
                                                options={CoinName}
                                            />
                                        </View>

                                        {/* WatchList Component */}
                                        <View style={{}}>
                                            < TouchableOpacity
                                                style={styles.buttonStyle}
                                                onPress={() => this.props.navigation.navigate("Watchlist")}
                                            // onPress={this.onPress}
                                            >
                                                <Icon
                                                    style={{ alignItems: 'center', justifyContent: 'center' }}
                                                    name='star-o'
                                                    size={21}
                                                    color='#ffffff'
                                                    backgroundColor='#f2f2f2'
                                                />
                                            </TouchableOpacity>
                                        </View>

                                        {/* Alert Component */}
                                        <View style={{}}>
                                            < TouchableOpacity
                                                style={styles.buttonStyle}
                                                onPress={() => this.props.navigation.navigate("Alert")}
                                            >
                                                <Icon
                                                    style={{ alignItems: 'center', justifyContent: 'center' }}
                                                    name='bell-o'
                                                    size={21}
                                                    color='#ffffff'
                                                    backgroundColor='#f2f2f2'
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            
                            </View>
                        </LinearGradient>

                        {/* Top Viewed Container  */}
                        <View style={{
                            display: this.state.topSearchCoinViewHeight === 0 ? 'none' : "flex",
                            alignItems: 'flex-start',
                            flexDirection: 'column',
                            borderBottomWidth: 1,
                            padding: 5,
                            borderColor: "#d9d9d9"
                        }}>
                            <View style={{ marginBottom: 5 }}>
                                <Text style={{ fontSize: 13, color: '#005580' }}>
                                    TOP VIEWED
                                </Text>
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: 'flex-start', }}>
                                <FlatList
                                    style={{ display: "flex", flexDirection: 'row' }}
                                    horizontal={true}
                                    data={dataList}
                                    showsHorizontalScrollIndicator={false}
                                    keyExtractor={(item, index) => index.toString()}
                                    ListFooterComponent={this.renderfooter}
                                    renderItem={({ item }) => (this.renderContent(item))}
                                />
                            </View>
                        </View>

                        {/* Coin List Cointainer */}
                        <View style={{ flex: 1, flexDirection: 'column', }}>
                            <View style={{ height: 40, flexDirection: 'row', padding: 10 }}>
                                <View style={{ flex: 1, alignItems: 'flex-start', flexDirection: 'row' }}>
                                    <View style={{ flex: 1 }}>
                                        <Text style={{ fontSize: 13, color: '#005580' }} onPress={() => this.sortList('name')}>NAME /</Text>
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={{ fontSize: 13, color: '#005580' }} onPress={() => this.sortList('market_cap_usd')}>   M.CAP</Text>
                                    </View>
                                </View>
                                <View style={{ flex: 1, alignItems: 'center', }}>
                                    <Text style={{ fontSize: 13, color: '#005580' }} onPress={() => this.sortList('percent_change24h')}>CHANGE</Text>
                                </View>

                                <View style={{ flex: 1, alignItems: 'center', }}>
                                    <Text style={{ fontSize: 13, color: '#005580' }} onPress={() => this.sortList('price')}>PRICE</Text>
                                </View>
                            </View>
                            <View style={{ flex: 1, flexDirection: "column" }}>
                                <FlatList
                                    windowSize={50}
                                    removeClippedSubviews={true}
                                    maxToRenderPerBatch={10}
                                    updateCellsBatchingPeriod={5}
                                    onScroll={this.onScroll}
                                    style={{ flex: 1 }}
                                    data={dataSource}
                                    keyExtractor={(item, index) => index.toString()}
                                    renderItem={({ item }) => (this.renderItemCoin(item))}
                                    onEndReached={this.handleLoadMore}
                                    onEndReachedThreshold={0}
                                    ListFooterComponent={this.renderItemCoinFooter}
                                />
                            </View>
                        </View>
                    </View>
                </SafeAreaView>
            </View>
        );
    }

    onShow = () => {
        this.setState({ visible: true });
    }

    onSelect = (picked) => {
        this.setState({
            picked: picked,
            visible: false
        })
    }

    onCancel = () => {
        this.setState({
            visible: false
        });
    }

}
const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: 'white',
    },
    loader: {
        marginTop: 10,
        alignItems: "center"
    },
    buttonStyle: {
        borderRadius: 12,
        shadowOpacity: 10,
        padding: 10,
        backgroundColor: '#2e5cb8',
    },
    PickerStyle: {
        width: "100%",
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#2e5cb8',
        borderRadius: 12,
        shadowOpacity: 10,
    },
    ListButtonStyle: {
        width: "100%",
        padding: 10,
        shadowOpacity: 10,
        height: 80,
        borderRadius: 15,
        borderWidth: 2,
        borderColor: '#3333cc',
        textAlign: 'center',
        justifyContent: 'center',
    },
})