import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {
  createMaterialTopTabNavigator,
  createAppContainer
} from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Home from './src/Home/index';
import ICOs from './src/ICOs/ICOs';
import Airdrops from './src/Airdrops/Airdrops';
import News from './src/News/index';
import Account from './src/Accounts/index';

const AppContainer = createMaterialTopTabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarLabel:"Home",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="home" size={23} color="#405BDB" />
        )
      },
    },
    ICOs: {
      screen: ICOs,
      navigationOptions: {
        tabBarLabel:"ICOs",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="file-invoice-dollar" size={23} color="#405BDB" />
        )
      },
    },
    Airdrops: {
      screen: Airdrops,
      navigationOptions: {
        tabBarLabel:"Airdrops",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="parachute-box" size={23} color="#405BDB" />
        )
      },
    },
    News: {
      screen: News,
      navigationOptions: {
        tabBarLabel:"News",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="newspaper" size={23} color="#405BDB" />
        )
      },
    },
    Account: {
      screen: Account,
      navigationOptions: {
        tabBarLabel:"Account",
        tabBarIcon: ({ tintColor }) => (
          <Icon name="user" size={23} color="#405BDB"  />
        )
      },
    },
  },
  {
    mode:"modal",
    initialRouteName: 'Home',
    tabBarPosition : 'bottom',
    swipeEnabled: true,
    animationEnabled: true,
    tabBarOptions: {
      showIcon: true,
      activeBackgroundColor: '#e6e6e6',
      inactiveBackgroundColor: '#e6e6e6',
      activeTintColor: '#405BDB',
      inactiveTintColor: '#a6a6a6', 
      upperCaseLabel : false,
      style : {
        backgroundColor: '#ffffff',
      },
      labelStyle: {
        fontWeight: 'bold',
        fontSize: 13,
        margin:0,
        padding:0,
        // fontStyle:'italic',
      }
    },
  },

);

export default createAppContainer(AppContainer);